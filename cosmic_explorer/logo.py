from manim import *

class Logo(Scene):
    def construct(self):
        # logo = SVGMobject("CEsplash_white_solid_lp5")

        circle_1_final = 0.24*UP+0.545*LEFT
        circle_2_final = 0.27*DOWN+0.09*RIGHT
        circle_1 = Circle(0.2, WHITE, fill_opacity=1).move_to(circle_1_final)
        circle_2 = Circle(0.15, WHITE, fill_opacity=1).move_to(circle_2_final)
        
        cosmic_font_size = 43
        explorer_font_size = 37
        cosmic_text = Text("COSMIC", font="Montserrat", weight=ULTRABOLD, font_size=cosmic_font_size).set_color(WHITE)
        explorer_text = Text("EXPLORER", font="Montserrat", weight=ULTRABOLD, font_size=explorer_font_size
                             ).set_color(WHITE).next_to(cosmic_text, DOWN, buff=0.1)
        
        circle_group = VGroup(circle_1, circle_2)
        text_group = VGroup(cosmic_text, explorer_text).move_to(ORIGIN)
        self.play(Create(circle_group))
        self.wait(0.5)
        self.play(Rotate(circle_1, angle=2*PI, about_point=circle_group.get_center()), Rotate(circle_2, angle=2*PI, about_point=circle_group.get_center()))
        self.wait(0.5)
        self.play(Write(text_group))

        # logo.set_color(WHITE)


class LogoInspiral(Scene):
    def construct(self):
        # logo = SVGMobject("CEsplash_white_solid_lp5")

        circle_1_final_pos = 0.24*UP+0.545*LEFT
        circle_2_final_pos = 0.27*DOWN+0.09*RIGHT
        circle_center = (circle_1_final_pos + circle_2_final_pos) / 2

        circle_1_start_pos = 20 * (circle_1_final_pos - circle_center) # Start off screen
        circle_2_start_pos = 30 * (circle_2_final_pos - circle_center)

        circle_1_start = Circle(0.2, WHITE, fill_opacity=1).move_to(circle_1_start_pos)
        circle_2_start = Circle(0.15, WHITE, fill_opacity=1).move_to(circle_2_start_pos)

        circle_1_final = Circle(0.2, WHITE, fill_opacity=1).move_to(circle_1_final_pos)
        circle_2_final = Circle(0.15, WHITE, fill_opacity=1).move_to(circle_2_final_pos)
        
        # CE Font
        cosmic_font_size = 43
        explorer_font_size = 37
        cosmic_text = Text("COSMIC", font="Montserrat", weight=ULTRABOLD, font_size=cosmic_font_size).set_color(WHITE)
        explorer_text = Text("EXPLORER", font="Montserrat", weight=ULTRABOLD, font_size=explorer_font_size
                             ).set_color(WHITE).next_to(cosmic_text, DOWN, buff=0.1)
        
        # Cubic Bezier for Arcs
        start_anchor_1 = circle_1_final_pos + 0.25 * UP + 1.27 * RIGHT
        start_handle_1 = start_anchor_1     + 0.5 * (np.cos(PI/2) * RIGHT + np.sin(PI/2) * UP)
        end_anchor_1   = circle_1_final_pos + 0.25 * UP + 0.2 * RIGHT
        end_handle_1   = end_anchor_1       + 0.5 * (np.cos(PI/4) * RIGHT + np.sin(PI/4) * UP)
        bezier_1 = CubicBezier(start_anchor_1, start_handle_1, end_handle_1, end_anchor_1)

        start_anchor_2 = circle_2_final_pos - 0.25 * UP - 1.27 * RIGHT
        start_handle_2 = start_anchor_2     - 0.5 * (np.cos(PI/2) * RIGHT + np.sin(PI/2) * UP)
        end_anchor_2   = circle_2_final_pos - 0.25 * UP - 0.2 * RIGHT
        end_handle_2   = end_anchor_2       - 0.5 * (np.cos(PI/4) * RIGHT + np.sin(PI/4) * UP)
        bezier_2 = CubicBezier(start_anchor_2, start_handle_2, end_handle_2, end_anchor_2)

        # Groups
        circle_group_start = VGroup(circle_1_start, circle_2_start)
        circle_group_final = VGroup(circle_1_final, circle_2_final)
        text_group = VGroup(cosmic_text, explorer_text).move_to(ORIGIN)

        
        # Start scene
        self.add(circle_group_start)
        self.play(
            Transform(
                circle_group_start,
                circle_group_final,
                path_func=utils.paths.path_along_circles(
                    4 * PI, circle_group_final.get_center()
                ),
                run_time=2.5,
                # rate_func=rate_functions.ease_in_quad
            )
        )
        self.wait(0.5)
        self.play(Write(text_group, run_time=2))
        self.wait(0.25)
        self.play(Create(bezier_1), Create(bezier_2))