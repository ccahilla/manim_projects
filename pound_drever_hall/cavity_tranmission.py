from operator import index
from manim import *
from copy import deepcopy

def get_cav_refl(phi, r1, r2):
    """Get the Fabry-Perot cavity amplitude reflection.
    phi is single-pass phase of the light.
    """
    return (-r1 + r2 * np.exp(2j * phi)) / (1 - r1 * r2 * np.exp(2j * phi))


def get_cav_buildup_toward_end_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 / (1 - r1 * r2 * np.exp(2j * phi))


def get_cav_buildup_toward_input_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 * r2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))


def get_cav_trans(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    t2 = np.sqrt(1 - r2**2)
    return t1 * t2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))


class CavityTransmission(Scene):
    """Plot the transmitted amplitude and phase of the cavity transmitted beam.
    As well as the phasor of this beam.
    """

    def get_phase(self, length, wavelength, frequency, time, phase):
        total_phase = 2 * PI * (length / wavelength - frequency * time) + phase
        return total_phase

    def sine_wave(self, xx, amp, wavelength, frequency, time, phase, x_origin):
        return amp * np.sin(
            2 * PI * ((xx - x_origin) / wavelength - frequency * time) + phase
        )

    def sine_wave_graph(
        self,
        amp,
        wavelength,
        frequency,
        time,
        phase=0.0,
        x_origin=0.0,
        x_range=None,
        color=RED,
    ):
        def sine_wave_caller(xx):
            sine = self.sine_wave(xx, amp, wavelength, frequency, time, phase, x_origin)
            return sine

        func = sine_wave_caller
        return FunctionGraph(func, x_range=x_range, color=color)

    def construct(self):
        ### Mirrors
        # Parameters
        mirror_width = 0.5

        x_input_mirror = ValueTracker(-3.0)
        x_end_mirror = ValueTracker(3.0)
        y_mirror = ValueTracker(0)

        input_mirror_refl = ValueTracker(0.8)
        end_mirror_refl = ValueTracker(0.8)

        # Objects
        input_mirror = always_redraw(
            lambda: Rectangle(
                height=3.2,
                width=mirror_width,
                fill_color=BLUE,
                fill_opacity=input_mirror_refl.get_value(),
                color=BLUE,
            ).shift(
                (x_input_mirror.get_value() - mirror_width / 2) * RIGHT
                + y_mirror.get_value() * UP
            )
        )

        end_mirror = always_redraw(
            lambda: Rectangle(
                height=3.2,
                width=mirror_width,
                fill_color=BLUE,
                fill_opacity=end_mirror_refl.get_value(),
                color=BLUE,
            ).shift(
                (x_end_mirror.get_value() + mirror_width / 2) * RIGHT
                + y_mirror.get_value() * UP
            )
        )

        ### Sine waves
        # Parameters
        time = ValueTracker(0.0)

        amplitude = ValueTracker(1.0)
        wavelength = ValueTracker(2.7)  # m
        frequency = ValueTracker(0.5)  # Hz

        input_phase = ValueTracker(0.0)  # rad
        input_sine_x_start = ValueTracker(-7.0)


        num_of_reflections = 2

        cavity_phase_fronts = [ValueTracker(0.0) for ii in range(num_of_reflections)]
        cavity_phase_backs = [ValueTracker(0.0) for ii in range(num_of_reflections)]

        for nn, cavity_phase_front in enumerate(cavity_phase_fronts):
            cavity_phase_front.index = nn
        for nn, cavity_phase_back in enumerate(cavity_phase_backs):
            cavity_phase_back.index = nn

        # Objects
        input_sine = self.sine_wave_graph(
            amplitude.get_value(),
            wavelength.get_value(),
            frequency.get_value(),
            time.get_value(),
            phase=input_phase.get_value(),
            x_origin=input_sine_x_start.get_value(),
            x_range=[input_sine_x_start.get_value(), x_input_mirror.get_value()],
        )
        
        cavity_sine_fronts = []
        cavity_sine_backs = []
        for nn in range(num_of_reflections):
            cavity_sine_front = VGroup()
            cavity_sine_front_graph = self.sine_wave_graph(
                amplitude.get_value(),
                wavelength.get_value(),
                frequency.get_value(),
                time.get_value(),
                phase=input_phase.get_value(),
                x_origin=x_input_mirror.get_value(),
                x_range=[x_input_mirror.get_value(), x_end_mirror.get_value()],
            )
            cavity_sine_front.direction = "front"
            cavity_sine_front.index = nn
            cavity_sine_front.color = RED
            cavity_sine_front.add(cavity_sine_front_graph)

            cavity_sine_fronts.append(cavity_sine_front)

        for nn in range(num_of_reflections):
            cavity_sine_back = VGroup()
            cavity_sine_back_graph = self.sine_wave_graph(
                amplitude.get_value(),
                -1 * wavelength.get_value(),
                frequency.get_value(),
                time.get_value(),
                phase=input_phase.get_value(),
                x_origin=x_end_mirror.get_value(),
                x_range=[x_input_mirror.get_value(), x_end_mirror.get_value()],
                color=RED_A,
            )
            cavity_sine_back.direction = "back"
            cavity_sine_back.index = nn
            cavity_sine_front.color = RED_A
            cavity_sine_back_graph.reverse_direction()
            cavity_sine_back.add(cavity_sine_back_graph)

            cavity_sine_backs.append(cavity_sine_back)

        ### Updaters

        ## Additions
        # must add the time ValueTracker to the scene
        self.add(time)
        self.add(wavelength)
        self.add(frequency)

        self.add(input_phase)

        self.add(*cavity_phase_fronts)
        self.add(*cavity_phase_backs)

        # Add updaters to the mobjects they update
        for nn, cavity_phase_front in enumerate(cavity_phase_fronts):
            print(f"index = {cavity_phase_front.index}")
            cavity_phase_front.add_updater(
                lambda mob: mob.set_value(
                    self.get_phase(
                        (x_input_mirror.get_value() - input_sine_x_start.get_value()) + (2 * mob.index) * (x_end_mirror.get_value() - x_input_mirror.get_value()),
                        wavelength.get_value(),
                        frequency.get_value(),
                        time.get_value(),
                        input_phase.get_value(),
                    )
                )
            )

        for nn, cavity_phase_back in enumerate(cavity_phase_backs):
            cavity_phase_back.add_updater(
                lambda mob: mob.set_value(
                    self.get_phase(
                        (x_input_mirror.get_value() - input_sine_x_start.get_value()) + (2 * mob.index + 1) * (x_end_mirror.get_value() - x_input_mirror.get_value()),
                        -1 * wavelength.get_value(),
                        frequency.get_value(),
                        time.get_value(),
                        input_phase.get_value() + np.pi,
                    )
                )
            )

        input_sine.add_updater(
            lambda mob: mob.become(
                self.sine_wave_graph(
                    amplitude.get_value(),
                    wavelength.get_value(),
                    frequency.get_value(),
                    time.get_value(),
                    phase=input_phase.get_value(),
                    x_origin=input_sine_x_start.get_value(),
                    x_range=[
                        input_sine_x_start.get_value(),
                        x_input_mirror.get_value(),
                    ],
                )
            )
        )

        def cavity_sine_updater(mob):
            if mob.direction == "front":
                cavity_phase = cavity_phase_fronts[mob.index]
                x_origin = x_input_mirror
            else:
                cavity_phase = cavity_phase_backs[mob.index]
                x_origin = x_end_mirror

            temp_mob = VGroup()
            temp_mob_graph = self.sine_wave_graph(
                amplitude.get_value(),
                wavelength.get_value(),
                frequency.get_value(),
                time.get_value(),
                phase=cavity_phase.get_value(),
                x_origin=x_origin.get_value(),
                x_range=[x_input_mirror.get_value(), x_end_mirror.get_value()],
                color=mob.color,
            )
            temp_mob.index = mob.index
            temp_mob.direction = mob.direction
            temp_mob.color = mob.color
            if mob.direction == "back":
                temp_mob_graph.reverse_direction()
            temp_mob.add(temp_mob_graph)

            mob.become(temp_mob)
            return 

        for cavity_sine_front in cavity_sine_fronts:
            cavity_sine_front.add_updater(cavity_sine_updater)

        for cavity_sine_back in cavity_sine_backs:
            cavity_sine_back.add_updater(cavity_sine_updater)

        ### Set up scene
        self.play(Create(input_mirror))
        self.wait(0.5)

        self.play(Create(end_mirror))
        self.wait(0.5)

        self.play(Create(input_sine), run_time=1, rate_func=rate_functions.linear)
        # self.wait(0.5)

        for cavity_sine_front, cavity_sine_back in zip(cavity_sine_fronts, cavity_sine_backs):
            self.play(Create(cavity_sine_front), run_time=1.5, rate_func=rate_functions.linear)
            self.play(Create(cavity_sine_back), run_time=1.5, rate_func=rate_functions.linear)
        self.wait(0.5)

        self.play(
            wavelength.animate.set_value(4),
            run_time=3,
            rate_func=rate_functions.ease_in_out_sine,
        )
        self.wait(0.2)

        self.play(
            x_end_mirror.animate.set_value(4),
            run_time=3,
            rate_func=rate_functions.ease_in_out_sine,
        )
        self.wait(0.2)

        return
