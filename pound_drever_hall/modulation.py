from manim import *

class Phasors(ThreeDScene):
    def construct(self):
        # self.camera.background_color = WHITE

        # Set up a timer that we can evolve at will by adding time_updater to the scene
        self.timer = 0.0 

        camera_phi = 0
        camera_theta = -90
        self.set_camera_orientation(phi=camera_phi * DEGREES, theta=camera_theta * DEGREES)
        
        phi, theta, focal_distance, gamma, zoom = self.camera.get_value_trackers()

        axes = ThreeDAxes()

        # Initial phasor
        main_amp = ValueTracker(2.5) 
        main_phi = ValueTracker(0) # degrees
        main_freq = ValueTracker(0.2) # Hz

        main_phasor = Arrow3D()
        main_phasor_color = BLUE
        def main_phasor_updater(mob: Mobject):
            xx = main_amp.get_value() * np.cos( 2 * PI * main_freq.get_value() * self.timer )
            yy = main_amp.get_value() * np.sin( 2 * PI * main_freq.get_value() * self.timer )
            temp_mob = Arrow3D(ORIGIN, xx*RIGHT+yy*UP).set_color(main_phasor_color)
            mob.become(temp_mob)
            return

        main_phasor.add_updater(main_phasor_updater)

        # Initial phasor circle
        main_phasor_circle = Circle().set_color(main_phasor_color)
        def main_phasor_circle_updater(mob: Mobject):
            temp_mob = Circle(radius=main_amp.get_value()).set_color(main_phasor_color)
            mob.become(temp_mob)
            return

        main_phasor_circle.add_updater(main_phasor_circle_updater)


        # Time as Decimal Number
        self.timer_value_tex = MathTex(
            r"t = ",
            substrings_to_isolate=r"t"
        ).shift(3*UP + 4*RIGHT)
        self.timer_value_tex.set_color_by_tex(r"t", YELLOW)
        self.timer_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1)
            .set_value(self.timer)
            .next_to(self.timer_value_tex, RIGHT, buff=0.15)
        )
        self.timer_value_group = VGroup(self.timer_value_tex, self.timer_value)


        ###   Construct Scene   ###
        self.rate = 1 # time update rate
        def time_updater(_, dt):
            self.timer += dt * self.rate
            return
    
        self.add(axes)
        self.add(main_amp)
        self.add(main_freq)

        self.play(FadeIn(main_phasor))

        self.play(Write(self.timer_value_group))

        main_phasor.add_updater(time_updater)
        self.wait(2*PI / main_freq.get_value())
        self.play(Create(main_phasor_circle))
        self.wait(2*PI / main_freq.get_value())
        main_phasor.remove_updater(time_updater)

        self.wait(1)

        # main_phasor.remove_updater(time_updater)



        # self.play(
        #     main_phi.animate(run_time=3, rate_func=linear).set_value(360), 
        # )
        # self.wait(1)

        self.begin_ambient_camera_rotation()

        self.play(
            main_amp.animate.set_value(4), run_time=1, #rate_func=linear
        )
        self.wait(1)
        self.play(
            main_amp.animate.set_value(1), run_time=1, #rate_func=linear
        )
        self.wait(1)
        self.play(
            main_amp.animate.set_value(3), run_time=1, #rate_func=linear
        )
        self.wait(1)
        
        

