from manim import *

class IntroChecklist(Scene):
    def construct(self):

        ## LIGO logo
        self.advanced_ligo = Tex(
            "LIGO", 
            tex_template=TexFontTemplates.libertine
        ).scale(2).move_to(2*UP + 5.5*RIGHT) 

        logo_scale = 0.4
        logo_ring_width = 0.1
        number_of_rings = 6
        start_ring_angle = 3 * PI / 2
        ring_angle = PI / 2
        ring_color = DARK_GREY
        arc_center = 3.5 * UP + 4 * RIGHT

        rings = []
        for ii in range(1, number_of_rings + 1):
            outer_radius = ii * logo_scale
            inner_radius = ii * logo_scale - logo_ring_width
            ring = AnnularSector(
                inner_radius=inner_radius, outer_radius=outer_radius, start_angle=start_ring_angle, angle=ring_angle, arc_center=arc_center, color=ring_color
            )
            rings.append(ring)

        ## Tex
        self.question_1 = Tex(
            "What will we learn in this video?",
            color=YELLOW_A
        ).move_to(3*UP + 6*LEFT, aligned_edge=LEFT)

        self.answer_1_a = Tex(
            "Gain an {{ intuition }} for how the LIGO detector"
        ).scale(0.7).next_to(self.question_1, DOWN, aligned_edge=LEFT)
        self.answer_1_a[1].set_color(PINK)

        self.answer_1_b = Tex(
            "generates a {{ laser power }} signal from a {{ gravitational wave }}"
        ).scale(0.7).next_to(self.answer_1_a, DOWN, aligned_edge=LEFT)
        self.answer_1_b[1].set_color(RED)
        self.answer_1_b[3].set_color(ORANGE)

        self.three_steps = Tex(
            "Three steps:",
            color=YELLOW_A
        ).next_to(self.answer_1_b, DOWN, aligned_edge=LEFT).shift(0.7*DOWN)

        self.step_1_a = Tex(
            "1. Briefly explore what a {{ gravitational wave }} does to the LIGO detector"
        ).scale(0.7).next_to(self.three_steps, DOWN, aligned_edge=LEFT)
        self.step_1_a[1].set_color(ORANGE)
        
        self.step_1_b = Tex(
            "(and everything else in the universe)"
        ).scale(0.7).next_to(self.step_1_a, DOWN, aligned_edge=LEFT)

        self.step_2 = Tex(
            "2. Review what a {{ laser beam }} is, and how a beam interacts with a {{ mirror }}"
        ).scale(0.7).next_to(self.step_1_b, DOWN, aligned_edge=LEFT).shift(0.5*DOWN)
        self.step_2[1].set_color(RED)
        self.step_2[3].set_color(BLUE)

        self.step_3_a = Tex(
            r"3. Build and understand a {{ Fabry P\'erot }} interferometer,"
        ).scale(0.7).next_to(self.step_2, DOWN, aligned_edge=LEFT).shift(0.5*DOWN)
        self.step_3_a[1].set_color(GREEN)

        self.step_3_b = Tex(
            r"and a {{ Michelson }} interferometer"
        ).scale(0.7).next_to(self.step_3_a, DOWN, aligned_edge=LEFT)
        self.step_3_b[1].set_color(RED)

        # Logo
        for ring in rings:
            self.add(ring)

        self.add(self.advanced_ligo)

        # Q1
        self.play(
            Write(self.question_1)
        )
        self.wait(2)
        self.play(
            Write(self.answer_1_a)
        )
        self.play(
            Write(self.answer_1_b)
        )
        self.wait(5)

        # Three steps
        self.play(
            Write(self.three_steps)
        )
        self.wait(3)

        self.play(
            Write(self.step_1_a)
        )
        self.play(
            Write(self.step_1_b)
        )
        self.wait(5)
        self.play(
            Write(self.step_2)
        )
        self.wait(5)
        self.play(
            Write(self.step_3_a)
        )
        self.play(
            Write(self.step_3_b)
        )
        self.wait(5)