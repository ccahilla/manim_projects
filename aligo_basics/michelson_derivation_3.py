"""michelson_derivation.py
Derives the power inside a Michelson interferometer.
"""

from manim import *

def unrotate(x1, x2, y1, y2):
    """Returns the angle and (x1, y1), (x2, y2) coordinates for rotation of the electric field.
    (this is a horrible way to do this but rn idk)
    """

    # get mean for rotation
    x0 = (x1 + x2)/2
    y0 = (y1 + y2)/2

    delta_x = x2 - x1
    delta_y = y2 - y1

    theta = np.arctan2(delta_y, delta_x)
    length = np.sqrt(delta_x**2 + delta_y**2)

    new_x1 = x0 - length/2
    new_x2 = x0 + length/2
    new_y1 = y0
    new_y2 = y0

    return new_x1, new_x2, new_y1, new_y2, theta

class MichelsonDerivation3(Scene):
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x + 2 * PI * freq * time + phase

    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x + 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
        y_start=None,
        y_end=None,
        stroke_width=1,
        rotate_angle=None,
        rotate_point=None
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end
        beam.y_start = y_start
        beam.y_end = y_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist for start and end pos
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        if y_start:
            start[1] = y_start.get_value()
        if y_end:
            end[1] = y_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color
        beam.stroke_width = stroke_width
        beam.rotate_angle = rotate_angle
        beam.rotate_point = rotate_point

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color, stroke_width=stroke_width)
            beam.add(temp_line)

        if rotate_angle:
            if not rotate_point:
                rotate_point = np.array([(start[0] + end[0])/2.0, (start[1] + end[1])/2.0, 0.0])
            beam.rotate(rotate_angle, about_point=rotate_point)

        return beam

    def sum_electric_fields(
        self, 
        field1, 
        field2, 
        color=RED, 
        reverse=False, 
        y_axis=None,
        stroke_width=None,
        sign_flip=False,
    ):
        """Add together two VGroups of electric fields.
        y_axis should be a ValueTracker defining the height of the summed wave.
        sign_flip flips the sign of the second beam being summed, useful for phase flip problems
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.field1 = field1
        beam.field2 = field2
        beam.color = color
        beam.reverse = reverse
        beam.y_axis = y_axis
        beam.stroke_width = stroke_width
        beam.sign_flip = sign_flip

        if field1.y_start:
            nominal_y1 = (field1.y_start.get_value() + field1.y_end.get_value())/2.0
        else:
            nominal_y1 = 0

        if field2.y_start:
            nominal_y2 = (field2.y_start.get_value() + field2.y_end.get_value())/2.0
        else:
            nominal_y2 = 0

        if y_axis:
            y_axis_value = y_axis.get_value()
        else:
            y_axis_value = (field1.y_start.get_value() + field1.y_end.get_value())/2.0

        if not stroke_width:
            stroke_width = 4
            beam.stroke_width = 4

        if beam.sign_flip:
            sign = -1
        else:
            sign = 1
        

        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = (line1.start[1] - nominal_y1) + sign * (line2.end[1] - nominal_y2) + y_axis_value
                new_line_end[1] = (line1.end[1] - nominal_y1) + sign * (line2.start[1] - nominal_y2) + y_axis_value
            else:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = (line1.start[1] - nominal_y1) + sign * (line2.start[1] - nominal_y2) + y_axis_value
                new_line_end[1] = (line1.end[1] - nominal_y1) + sign * (line2.end[1] - nominal_y2) + y_axis_value

            new_line = Line(new_line_start, new_line_end, color=color, stroke_width=stroke_width)
            beam.add(new_line)

        # rotate
        if field1.rotate_angle:
            if not field1.rotate_point:
                rotate_point = np.array(
                    [(field1.x_start.get_value() + field1.x_end.get_value())/2.0, (field1.y_start.get_value() + field1.y_end.get_value())/2.0, 0.0]
                )
            else:
                rotate_point = field1.rotate_point
            beam.rotate(field1.rotate_angle)


        return beam


    def construct(self):
        
        self.color_map = {
            "E_in" : RED,
            "E_x1" : RED_A,
            "E_y1" : RED_E,
            "E_x2" : ORANGE,
            "E_y2" : PINK,
            "E_refl" : YELLOW,
            "E_refl_x" : YELLOW_A,
            "E_refl_y" : YELLOW_E,
            "E_trans" : GREEN,
            "E_trans_x" : PURPLE,
            "E_trans_y" : GREY,
            "mirror" : BLUE,
            "lc" : TEAL,
            "ld" : MAROON,
        }

        self.timer = 0.0
        self.rate = 0.5

        y_main_beam = 0.0
        number_of_lines = 150
        stroke_width = 3

        ## Mirrors
        # Beamsplitter
        bs_refl = ValueTracker(1/np.sqrt(2))
        bs_trans = ValueTracker(1/np.sqrt(2))
        bs_trans.add_updater(
            lambda mob: mob.set_value(np.sqrt(1 - bs_refl.get_value()**2))
        )
        bs_height = 2.5
        bs_width = 0.3
        bs_origin = ORIGIN
        bs_angle = 3*PI/4
        self.bs = Rectangle(
            color=BLUE, height=bs_height, width=bs_width, fill_opacity=bs_refl.get_value()**2,
        ).shift(bs_origin - bs_width/2.0 * RIGHT).rotate(bs_angle, about_point=bs_origin)

        mirror_height = 1.2
        x_mirror_length = ValueTracker(3.0)
        x_mirror_origin = 3.0 * RIGHT
        self.x_mirror = Rectangle(
            color=BLUE, height=mirror_height, width=bs_width, fill_opacity=1.0,
        ).shift(x_mirror_origin + bs_width/2.0 * RIGHT)
        self.x_mirror.add_updater(
            lambda mob: mob.move_to((x_mirror_length.get_value() + bs_width/2.0) * RIGHT)
        )

        y_mirror_angle = PI/2
        y_mirror_length = ValueTracker(3.0)
        y_mirror_origin = 3 * UP
        self.y_mirror = Rectangle(
            color=BLUE, height=mirror_height, width=bs_width, fill_opacity=1.0,
        ).shift(y_mirror_origin + bs_width/2.0 * RIGHT).rotate(y_mirror_angle, about_point=y_mirror_origin)
        self.y_mirror.add_updater(
            lambda mob: mob.move_to((y_mirror_length.get_value() + bs_width/2.0) * UP)
        )

        ## Laser
        laser_height = 1.2
        laser_width = 2.0
        laser_origin = 5 * LEFT
        self.laser = Rectangle(
            color=RED_A, height=laser_height, width=laser_width, fill_opacity=0.3,
        ).move_to(laser_origin, aligned_edge=RIGHT)

        ## Photodetector
        pd_origin = np.array([0.0, -3.0, 0.0])
        self.photodetector = Sector(start_angle=PI, angle=PI, arc_center=pd_origin, outer_radius=0.6, fill_opacity=1.0, color=GREEN)

        ## Propagating Electric Fields
        # Input Beam
        amp_input = ValueTracker(0.4)
        k_input = ValueTracker(PI)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        x_start_input = ValueTracker(-5.0)
        x_end_input = ValueTracker(0.0)
        y_start_input = ValueTracker(y_main_beam)
        y_end_input = ValueTracker(y_main_beam)

        start_input = np.array([x_start_input.get_value(), 0.0, 0.0])
        end_input = np.array([x_end_input.get_value(), 0.0, 0.0])

        rotate_angle_input = 0

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            color=self.color_map["E_in"],
            x_start=x_start_input,
            x_end=x_end_input,
            y_start=y_start_input,
            y_end=y_end_input,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_input, # this arg not optional apparently in my spagetti code
        )

        # X-arm forward beam
        amp_x_arm_1 = amp_input
        k_x_arm_1 = k_input
        freq_x_arm_1 = freq_input
        time_x_arm_1 = 0.0
        phase_x_arm_1 = phase_input

        x_start_x_arm_1 = x_end_input
        x_end_x_arm_1 = x_mirror_length
        y_start_x_arm_1 = ValueTracker(y_main_beam)
        y_end_x_arm_1 = ValueTracker(y_main_beam)
        
        start_x_arm_1 = np.array([x_start_x_arm_1.get_value(), y_start_x_arm_1.get_value(), 0.0])
        end_x_arm_1 = np.array([x_end_x_arm_1.get_value(), y_end_x_arm_1.get_value(), 0.0])

        rotate_angle_x_arm_1 = 0

        amp_x_arm_1_scaler = bs_trans
        k_x_arm_1_scaler = 1
        freq_x_arm_1_scaler = 1
        phase_x_arm_1_offset = ValueTracker(0.0)
        phase_x_arm_1_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.x_arm_1_beam = self.make_electric_field(
            amp_x_arm_1, 
            k_x_arm_1, 
            freq_x_arm_1, 
            time_x_arm_1, 
            phase_x_arm_1, 
            start_x_arm_1, 
            end_x_arm_1, 
            number_of_lines,
            color=self.color_map["E_x1"],
            amp_scaler=amp_x_arm_1_scaler,
            k_scaler=k_x_arm_1_scaler,
            freq_scaler=freq_x_arm_1_scaler,
            phase_offset=phase_x_arm_1_offset,
            x_start=x_start_x_arm_1,
            x_end=x_end_x_arm_1,
            y_start=y_start_x_arm_1,
            y_end=y_end_x_arm_1,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_x_arm_1
        )

        # X-arm backward beam
        amp_x_arm_2 = amp_input
        k_x_arm_2 = k_input
        freq_x_arm_2 = freq_input
        time_x_arm_2 = 0.0
        phase_x_arm_2 = phase_input

        x_start_x_arm_2 = x_mirror_length
        x_end_x_arm_2 = x_end_input
        y_start_x_arm_2 = ValueTracker(y_main_beam)
        y_end_x_arm_2 = ValueTracker(y_main_beam)
        
        start_x_arm_2 = np.array([x_start_x_arm_2.get_value(), y_start_x_arm_2.get_value(), 0.0])
        end_x_arm_2 = np.array([x_end_x_arm_2.get_value(), y_end_x_arm_2.get_value(), 0.0])

        rotate_angle_x_arm_2 = PI

        amp_x_arm_2_scaler = amp_x_arm_1_scaler
        k_x_arm_2_scaler = 1
        freq_x_arm_2_scaler = 1
        phase_x_arm_2_offset = ValueTracker(0.0)
        phase_x_arm_2_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_x_arm_1.get_value() - x_start_input.get_value() + x_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.x_arm_2_beam = self.make_electric_field(
            amp_x_arm_2, 
            k_x_arm_2, 
            freq_x_arm_2, 
            time_x_arm_2, 
            phase_x_arm_2, 
            start_x_arm_2, 
            end_x_arm_2, 
            number_of_lines,
            color=self.color_map["E_x2"],
            amp_scaler=amp_x_arm_2_scaler,
            k_scaler=k_x_arm_2_scaler,
            freq_scaler=freq_x_arm_2_scaler,
            phase_offset=phase_x_arm_2_offset,
            x_start=x_start_x_arm_2,
            x_end=x_end_x_arm_2,
            y_start=y_start_x_arm_2,
            y_end=y_end_x_arm_2,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_x_arm_2
        )

        # Y-arm forward beam
        amp_y_arm_1 = amp_input
        k_y_arm_1 = k_input
        freq_y_arm_1 = freq_input
        time_y_arm_1 = 0.0
        phase_y_arm_1 = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_main_beam, y_mirror_length.get_value())

        x_start_y_arm_1 = ValueTracker(x1)
        x_end_y_arm_1 = ValueTracker(x2)
        y_start_y_arm_1 = ValueTracker(y1)
        y_end_y_arm_1 = ValueTracker(y2)
        
        start_y_arm_1 = np.array([x_start_y_arm_1.get_value(), y_start_y_arm_1.get_value(), 0.0])
        end_y_arm_1 = np.array([x_end_y_arm_1.get_value(), y_end_y_arm_1.get_value(), 0.0])

        rotate_angle_y_arm_1 = theta

        amp_y_arm_1_scaler = bs_refl
        k_y_arm_1_scaler = 1
        freq_y_arm_1_scaler = 1
        phase_y_arm_1_offset = ValueTracker(0.0)
        phase_y_arm_1_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.y_arm_1_beam = self.make_electric_field(
            amp_y_arm_1, 
            k_y_arm_1, 
            freq_y_arm_1, 
            time_y_arm_1, 
            phase_y_arm_1, 
            start_y_arm_1, 
            end_y_arm_1, 
            number_of_lines,
            color=self.color_map["E_y1"],
            amp_scaler=amp_y_arm_1_scaler,
            k_scaler=k_y_arm_1_scaler,
            freq_scaler=freq_y_arm_1_scaler,
            phase_offset=phase_y_arm_1_offset,
            x_start=x_start_y_arm_1,
            x_end=x_end_y_arm_1,
            y_start=y_start_y_arm_1,
            y_end=y_end_y_arm_1,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_y_arm_1
        )

        # Y-arm backward beam
        amp_y_arm_2 = amp_input
        k_y_arm_2 = k_input
        freq_y_arm_2 = freq_input
        time_y_arm_2 = 0.0
        phase_y_arm_2 = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_mirror_length.get_value(), 0.0)

        x_start_y_arm_2 = ValueTracker(x1)
        x_end_y_arm_2 = ValueTracker(x2)
        y_start_y_arm_2 = ValueTracker(y1)
        y_end_y_arm_2 = ValueTracker(y2)
        
        start_y_arm_2 = np.array([x_start_y_arm_2.get_value(), y_start_y_arm_2.get_value(), 0.0])
        end_y_arm_2 = np.array([x_end_y_arm_2.get_value(), y_end_y_arm_2.get_value(), 0.0])

        rotate_angle_y_arm_2 = theta 

        amp_y_arm_2_scaler = amp_y_arm_1_scaler
        k_y_arm_2_scaler = 1
        freq_y_arm_2_scaler = 1
        phase_y_arm_2_offset = ValueTracker(PI)
        phase_y_arm_2_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value() + y_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.y_arm_2_beam = self.make_electric_field(
            amp_y_arm_2, 
            k_y_arm_2, 
            freq_y_arm_2, 
            time_y_arm_2, 
            phase_y_arm_2, 
            start_y_arm_2, 
            end_y_arm_2, 
            number_of_lines,
            color=self.color_map["E_y2"],
            amp_scaler=amp_y_arm_2_scaler,
            k_scaler=k_y_arm_2_scaler,
            freq_scaler=freq_y_arm_2_scaler,
            phase_offset=phase_y_arm_2_offset,
            x_start=x_start_y_arm_2,
            x_end=x_end_y_arm_2,
            y_start=y_start_y_arm_2,
            y_end=y_end_y_arm_2,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_y_arm_2
        )

        # Reflected beam
        # is the sum of the beams from the x- and y-arms
        # REFL Y
        amp_refl_y = amp_input
        k_refl_y = k_input
        freq_refl_y = freq_input
        time_refl_y = 0.0
        phase_refl_y = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, x_start_input.get_value(), y_main_beam, y_main_beam)

        x_start_refl_y = ValueTracker(x1)
        x_end_refl_y = ValueTracker(x2)
        y_start_refl_y = ValueTracker(y1)
        y_end_refl_y = ValueTracker(y2)
        
        start_refl_y = np.array([x_start_refl_y.get_value(), y_start_refl_y.get_value(), 0.0])
        end_refl_y = np.array([x_end_refl_y.get_value(), y_end_refl_y.get_value(), 0.0])

        rotate_angle_refl_y = theta

        amp_refl_y_scaler = ValueTracker(1.0)
        amp_refl_y_scaler.add_updater(
            lambda mob: mob.set_value(bs_refl.get_value()**2)
        )
        k_refl_y_scaler = 1
        freq_refl_y_scaler = 1
        phase_refl_y_offset = ValueTracker(0.0)
        phase_refl_y_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value() + 2.0*y_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.refl_y_beam = self.make_electric_field(
            amp_refl_y, 
            k_refl_y, 
            freq_refl_y, 
            time_refl_y, 
            phase_refl_y, 
            start_refl_y, 
            end_refl_y, 
            number_of_lines,
            color=self.color_map["E_refl_y"],
            amp_scaler=amp_refl_y_scaler,
            k_scaler=k_refl_y_scaler,
            freq_scaler=freq_refl_y_scaler,
            phase_offset=phase_refl_y_offset,
            x_start=x_start_refl_y,
            x_end=x_end_refl_y,
            y_start=y_start_refl_y,
            y_end=y_end_refl_y,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_refl_y
        )

        # REFL X
        amp_refl_x = amp_input
        k_refl_x = k_input
        freq_refl_x = freq_input
        time_refl_x = 0.0
        phase_refl_x = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, x_start_input.get_value(), y_main_beam, y_main_beam)

        x_start_refl_x = ValueTracker(x1)
        x_end_refl_x = ValueTracker(x2)
        y_start_refl_x = ValueTracker(y1)
        y_end_refl_x = ValueTracker(y2)
        
        start_refl_x = np.array([x_start_refl_x.get_value(), y_start_refl_x.get_value(), 0.0])
        end_refl_x = np.array([x_end_refl_x.get_value(), y_end_refl_x.get_value(), 0.0])

        rotate_angle_refl_x = theta

        amp_refl_x_scaler = ValueTracker(1.0)
        amp_refl_x_scaler.add_updater(
            lambda mob: mob.set_value(bs_trans.get_value()**2)
        )
        k_refl_x_scaler = 1
        freq_refl_x_scaler = 1
        phase_refl_x_offset = ValueTracker(0.0)
        phase_refl_x_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value() + 2.0*x_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.refl_x_beam = self.make_electric_field(
            amp_refl_x, 
            k_refl_x, 
            freq_refl_x, 
            time_refl_x, 
            phase_refl_x, 
            start_refl_x, 
            end_refl_x, 
            number_of_lines,
            color=self.color_map["E_refl_x"],
            amp_scaler=amp_refl_x_scaler,
            k_scaler=k_refl_x_scaler,
            freq_scaler=freq_refl_x_scaler,
            phase_offset=phase_refl_x_offset,
            x_start=x_start_refl_x,
            x_end=x_end_refl_x,
            y_start=y_start_refl_x,
            y_end=y_end_refl_x,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_refl_x
        )

        # Transmitted beam
        # is the sum of the beams from the x- and y-arms
        # Trans Y
        amp_trans_y = amp_input
        k_trans_y = k_input
        freq_trans_y = freq_input
        time_trans_y = 0.0
        phase_trans_y = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_main_beam, -3.0)

        x_start_trans_y = ValueTracker(x1)
        x_end_trans_y = ValueTracker(x2)
        y_start_trans_y = ValueTracker(y1)
        y_end_trans_y = ValueTracker(y2)
        
        start_trans_y = np.array([x_start_trans_y.get_value(), y_start_trans_y.get_value(), 0.0])
        end_trans_y = np.array([x_end_trans_y.get_value(), y_end_trans_y.get_value(), 0.0])

        rotate_angle_trans_y = theta

        amp_trans_y_scaler = ValueTracker(1.0)
        amp_trans_y_scaler.add_updater(
            lambda mob: mob.set_value(bs_refl.get_value() * bs_trans.get_value())
        )
        k_trans_y_scaler = 1
        freq_trans_y_scaler = 1
        phase_trans_y_offset = ValueTracker(0.0)
        phase_trans_y_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value() + 2.0*y_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.trans_y_beam = self.make_electric_field(
            amp_trans_y, 
            k_trans_y, 
            freq_trans_y, 
            time_trans_y, 
            phase_trans_y, 
            start_trans_y, 
            end_trans_y, 
            number_of_lines,
            color=self.color_map["E_trans_y"],
            amp_scaler=amp_trans_y_scaler,
            k_scaler=k_trans_y_scaler,
            freq_scaler=freq_trans_y_scaler,
            phase_offset=phase_trans_y_offset,
            x_start=x_start_trans_y,
            x_end=x_end_trans_y,
            y_start=y_start_trans_y,
            y_end=y_end_trans_y,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_trans_y
        )

        # Trans X
        amp_trans_x = amp_input
        k_trans_x = k_input
        freq_trans_x = freq_input
        time_trans_x = 0.0
        phase_trans_x = phase_input

        x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_main_beam, -3.0)

        x_start_trans_x = ValueTracker(x1)
        x_end_trans_x = ValueTracker(x2)
        y_start_trans_x = ValueTracker(y1)
        y_end_trans_x = ValueTracker(y2)
        
        start_trans_x = np.array([x_start_trans_x.get_value(), y_start_trans_x.get_value(), 0.0])
        end_trans_x = np.array([x_end_trans_x.get_value(), y_end_trans_x.get_value(), 0.0])

        rotate_angle_trans_x = theta

        amp_trans_x_scaler = ValueTracker(1.0)
        amp_trans_x_scaler.add_updater(
            lambda mob: mob.set_value(-1 * bs_refl.get_value() * bs_trans.get_value())
        )
        k_trans_x_scaler = 1
        freq_trans_x_scaler = 1
        phase_trans_x_offset = ValueTracker(0.0)
        phase_trans_x_offset.add_updater(
            lambda mob: mob.set_value( 
                self.get_phase(
                    k_input.get_value(), 
                    x_end_input.get_value() - x_start_input.get_value() + 2.0*x_mirror_length.get_value(), 
                    freq_input.get_value(), 
                    time=0, # this is already account for in wave_updater
                    phase=0 # this is already accounted for in phase_refl
                ) 
            )
        )

        self.trans_x_beam = self.make_electric_field(
            amp_trans_x, 
            k_trans_x, 
            freq_trans_x, 
            time_trans_x, 
            phase_trans_x, 
            start_trans_x, 
            end_trans_x, 
            number_of_lines,
            color=self.color_map["E_trans_x"],
            amp_scaler=amp_trans_x_scaler,
            k_scaler=k_trans_x_scaler,
            freq_scaler=freq_trans_x_scaler,
            phase_offset=phase_trans_x_offset,
            x_start=x_start_trans_x,
            x_end=x_end_trans_x,
            y_start=y_start_trans_x,
            y_end=y_end_trans_x,
            stroke_width=stroke_width,
            rotate_angle=rotate_angle_trans_x
        )


        ## Standing Waves
        self.x_arm_beam = self.sum_electric_fields(self.x_arm_1_beam, self.x_arm_2_beam, color=WHITE, reverse=True, stroke_width=4)
        self.y_arm_beam = self.sum_electric_fields(self.y_arm_1_beam, self.y_arm_2_beam, color=WHITE, reverse=True, stroke_width=4, sign_flip=True)
        self.refl_beam = self.sum_electric_fields(self.refl_x_beam, self.refl_y_beam, color=self.color_map["E_refl"], reverse=False, stroke_width=4)
        self.trans_beam = self.sum_electric_fields(self.trans_x_beam, self.trans_y_beam, color=self.color_map["E_trans"], reverse=False, stroke_width=4)


        ## Arrows
        self.input_arrow = Arrow(
            start=LEFT,
            end=RIGHT,
            color=self.color_map["E_in"]
        ).next_to(self.input_beam, UP).shift(0.3*DOWN)

        self.refl_arrow = Arrow(
            start=RIGHT,
            end=LEFT,
            color=self.color_map["E_refl"]
        ).next_to(self.input_beam, DOWN)

        self.x_arm_1_arrow = Arrow(
            start=LEFT,
            end=RIGHT,
            color=self.color_map["E_x1"]
        ).next_to(self.x_arm_1_beam, UP).shift(0.3*DOWN + 0.3*RIGHT)

        self.x_arm_2_arrow = Arrow(
            start=RIGHT,
            end=LEFT,
            color=self.color_map["E_x2"]
        ).next_to(self.x_arm_2_beam, DOWN)

        self.y_arm_1_arrow = Arrow(
            start=DOWN,
            end=UP,
            color=self.color_map["E_y1"]
        ).next_to(self.y_arm_1_beam, LEFT).shift(0.3*UP)

        self.y_arm_2_arrow = Arrow(
            start=UP,
            end=DOWN,
            color=self.color_map["E_y2"]
        ).next_to(self.y_arm_2_beam, RIGHT).shift(0.3*LEFT)

        self.trans_arrow = Arrow(
            start=UP,
            end=DOWN,
            color=self.color_map["E_trans"]
        ).next_to(self.trans_beam, RIGHT).shift(0.5*(DOWN+LEFT))

        ## Tex
        self.laser_tex = Tex(
            "Laser", 
            color=self.color_map["E_in"]
        ).next_to(self.laser, UP)

        self.photodetector_tex = Tex(
            "Photodetector", 
            color=GREEN
        ).next_to(self.photodetector, RIGHT)

        self.x_mirror_tex = Tex(
            "X-mirror", 
            color=BLUE
        ).next_to(self.x_mirror, DOWN)
        self.x_mirror_tex.add_updater(
            lambda mob: mob.next_to(self.x_mirror, DOWN)
        )

        self.y_mirror_tex = Tex(
            "Y-mirror", 
            color=BLUE
        ).next_to(self.y_mirror, LEFT)

        # Change of basis
        self.change_of_basis_tex = Tex(
            "Change of basis:"
        ).move_to(2.0*DOWN+2.0*RIGHT)

        self.change_of_basis_eq_1 = MathTex(
            r"{{ \ell_c }} = { {{ \ell_x }} + {{ \ell_y }} \over 2 }"
        ).next_to(self.change_of_basis_tex, DOWN)
        self.change_of_basis_eq_2 = MathTex(
            r"{{ \ell_d }} = { {{ \ell_x }} - {{ \ell_y }} \over 2 }"
        ).next_to(self.change_of_basis_eq_1, DOWN)

        self.change_of_basis_eq_x = MathTex(
            r"{{ \ell_x }} = {{ \ell_c }} + {{ \ell_d }}"
        ).move_to(self.change_of_basis_eq_1)
        self.change_of_basis_eq_y = MathTex(
            r"{{ \ell_y }} = {{ \ell_c }} - {{ \ell_d }}"
        ).move_to(self.change_of_basis_eq_2)

        # Arm lengths Tex
        self.meters_y = Tex("m")

        # X arm
        self.x_arm_length_tex = MathTex(
            r"\ell_x = "
        ).scale(0.7)
        self.x_arm_length_decimalnumber = DecimalNumber(
            number=x_mirror_length.get_value(),
            num_decimal_places=2
        ).scale(0.7).next_to(self.x_arm_length_tex, RIGHT)
        
        self.x_arm_length_decimalnumber.add_updater(
            lambda mob: mob.set_value(x_mirror_length.get_value())
        )

        self.meters_x = Tex("m").scale(0.7).next_to(self.x_arm_length_decimalnumber, RIGHT).align_to(self.x_arm_length_decimalnumber, DOWN)

        self.x_arm_length_group = VGroup(
            self.x_arm_length_tex,
            self.x_arm_length_decimalnumber,
            self.meters_x
        )
        self.x_arm_length_group.add_updater(
            lambda mob: mob.next_to(self.x_arm_1_beam, DOWN)
        )

        # Y arm
        self.y_arm_length_tex = MathTex(
            r"\ell_y = "
        ).scale(0.7)
        self.y_arm_length_decimalnumber = DecimalNumber(
            number=y_mirror_length.get_value(),
            num_decimal_places=2
        ).scale(0.7).next_to(self.y_arm_length_tex, RIGHT)
        
        self.y_arm_length_decimalnumber.add_updater(
            lambda mob: mob.set_value(y_mirror_length.get_value())
        )

        self.meters_y = Tex("m").scale(0.7).next_to(self.y_arm_length_decimalnumber, RIGHT).align_to(self.y_arm_length_decimalnumber, DOWN)

        self.y_arm_length_group = VGroup(
            self.y_arm_length_tex,
            self.y_arm_length_decimalnumber,
            self.meters_y
        )
        self.y_arm_length_group.add_updater(
            lambda mob: mob.next_to(self.y_arm_1_beam, LEFT)
        )

        # Fields
        self.input_beam_tex = MathTex(
            r"E_\mathrm{in}",
            color=self.color_map["E_in"]
        ).next_to(self.input_beam, UP)

        self.x_arm_2_beam_tex = MathTex(
            r"E_x",
            color=self.color_map["E_x2"]
        ).next_to(self.x_arm_2_beam, UP)

        self.y_arm_2_beam_tex = MathTex(
            r"E_y",
            color=self.color_map["E_y2"]
        ).next_to(self.y_arm_2_beam, RIGHT)

        self.refl_beam_tex = MathTex(
            r"E_\mathrm{refl}"
        ).next_to(self.refl_beam, DOWN)
        self.refl_beam_tex[0].set_color(self.color_map["E_refl"])

        self.trans_beam_tex = MathTex(
            r"E_\mathrm{trans}"
        ).next_to(self.trans_beam, RIGHT).shift(0.8*DOWN)
        self.trans_beam_tex[0].set_color(self.color_map["E_trans"])

        # Equations
        self.x_arm_eq = MathTex(
            r"{{ E_x }} = {{ \tau }} e^{i 2 k \ell_x} {{ E_\mathrm{in} }}"
        ).scale(0.8).move_to(3.5*UP + 5*RIGHT)
        self.x_arm_eq[0].set_color(self.color_map["E_x2"])
        self.x_arm_eq[2].set_color(self.color_map["mirror"])
        self.x_arm_eq[4].set_color(self.color_map["E_in"])

        self.y_arm_eq = MathTex(
            r"{{ E_y }} = {{ r }} e^{i 2 k \ell_y} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(self.x_arm_eq, DOWN)
        self.y_arm_eq[0].set_color(self.color_map["E_y2"])
        self.y_arm_eq[2].set_color(self.color_map["mirror"])
        self.y_arm_eq[4].set_color(self.color_map["E_in"])

        self.refl_eq_1 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ \tau }}{{ E_x }}{{ + r }}{{ E_y }}"
        ).scale(0.8).next_to(self.y_arm_eq, DOWN)
        self.refl_eq_1[0].set_color(self.color_map["E_refl"])
        self.refl_eq_1[2].set_color(self.color_map["mirror"])
        self.refl_eq_1[3].set_color(self.color_map["E_x2"])
        self.refl_eq_1[4].set_color(self.color_map["mirror"])
        self.refl_eq_1[5].set_color(self.color_map["E_y2"])

        self.trans_eq_1 = MathTex(
            r"{{ E_\mathrm{trans} }} = {{ - r }}{{ E_x }} + {{ \tau }}{{ E_y }}"
        ).scale(0.8).next_to(self.refl_eq_1, DOWN)
        self.trans_eq_1[0].set_color(self.color_map["E_trans"])
        self.trans_eq_1[2].set_color(self.color_map["mirror"])
        self.trans_eq_1[3].set_color(self.color_map["E_x2"])
        self.trans_eq_1[5].set_color(self.color_map["mirror"])
        self.trans_eq_1[6].set_color(self.color_map["E_y2"])

        # Equation locations
        refl_eq_location = 2.5*UP + 3.5*LEFT
        trans_eq_location = 2.5*UP + 3.5*RIGHT

        # Further refl eqs
        self.refl_eq_2 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ \tau }}^2 {{ e^{i 2 k \ell_x} }} {{ E_\mathrm{in} }} + {{ r }}^2 {{ e^{i 2 k \ell_y} }} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(refl_eq_location, DOWN).shift(0.5*DOWN)
        self.refl_eq_2[0].set_color(self.color_map["E_refl"])
        self.refl_eq_2[2].set_color(self.color_map["mirror"])
        self.refl_eq_2[6].set_color(self.color_map["E_in"])
        self.refl_eq_2[8].set_color(self.color_map["mirror"])
        self.refl_eq_2[12].set_color(self.color_map["E_in"])

        self.refl_eq_3 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2} ( {{ e^{i 2 k \ell_x} }} + {{ e^{i 2 k \ell_y} }} ) "
        ).scale(0.8).next_to(self.refl_eq_2, DOWN)
        self.refl_eq_3[0].set_color(self.color_map["E_refl"])
        self.refl_eq_3[2].set_color(self.color_map["E_in"])

        self.refl_eq_4 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2}{{ ( }}e^{ i 2 k ({{ \ell_c }} + {{ \ell_d }}) }{{ + }}e^{ i 2 k ({{ \ell_c }} - {{ \ell_d }}) } ) "
        ).scale(0.8).next_to(self.refl_eq_3, DOWN)
        self.refl_eq_4[0].set_color(self.color_map["E_refl"])
        self.refl_eq_4[2].set_color(self.color_map["E_in"])
        self.refl_eq_4[6].set_color(self.color_map["lc"])
        self.refl_eq_4[8].set_color(self.color_map["ld"])
        self.refl_eq_4[12].set_color(self.color_map["lc"])
        self.refl_eq_4[14].set_color(self.color_map["ld"])

        self.refl_eq_5 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ E_\mathrm{in} }} \over 2}{{ }}e^{i 2 k {{ \ell_c }}}{{ ( }}e^{ i 2 k {{ \ell_d }} }{{ + }}e^{-i 2 k {{ \ell_d }} } ) "
        ).scale(0.8).next_to(self.refl_eq_4, DOWN)
        self.refl_eq_5[0].set_color(self.color_map["E_refl"])
        self.refl_eq_5[2].set_color(self.color_map["E_in"])
        self.refl_eq_5[6].set_color(self.color_map["lc"])
        self.refl_eq_5[10].set_color(self.color_map["ld"])
        self.refl_eq_5[14].set_color(self.color_map["ld"])

        self.refl_eq_7 = MathTex(
            r"{ {{ E_\mathrm{refl} }} \over {{ E_\mathrm{in} }} }{{ = }}e^{ i 2 k {{ \ell_c }} }{{ }}\cos{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.refl_eq_5, DOWN)
        self.refl_eq_7[1].set_color(self.color_map["E_refl"])
        self.refl_eq_7[3].set_color(self.color_map["E_in"])
        self.refl_eq_7[7].set_color(self.color_map["lc"])
        self.refl_eq_7[11].set_color(self.color_map["ld"])


        # Further trans eqs
        self.trans_eq_2 = MathTex(
            r"{{ E_\mathrm{trans} }} = {{ - r }}{{ \tau }}{{ e^{i 2 k \ell_x} }} {{ E_\mathrm{in} }} + {{ r }}{{ \tau }}{{ e^{i 2 k \ell_y} }} {{ E_\mathrm{in} }}"
        ).scale(0.8).next_to(trans_eq_location, DOWN).shift(0.5*DOWN)
        self.trans_eq_2[0].set_color(self.color_map["E_trans"])
        self.trans_eq_2[2].set_color(self.color_map["mirror"])
        self.trans_eq_2[3].set_color(self.color_map["mirror"])
        self.trans_eq_2[6].set_color(self.color_map["E_in"])
        self.trans_eq_2[8].set_color(self.color_map["mirror"])
        self.trans_eq_2[9].set_color(self.color_map["mirror"])
        self.trans_eq_2[12].set_color(self.color_map["E_in"])

        self.trans_eq_3 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2} ( - {{ e^{i 2 k \ell_x} }} + {{ e^{i 2 k \ell_y} }} ) "
        ).scale(0.8).next_to(self.trans_eq_2, DOWN)
        self.trans_eq_3[0].set_color(self.color_map["E_trans"])
        self.trans_eq_3[2].set_color(self.color_map["E_in"])

        self.trans_eq_4 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2}{{ ( }}-e^{ i 2 k ({{ \ell_c }} + {{ \ell_d }}) }{{ + }}e^{ i 2 k ({{ \ell_c }} - {{ \ell_d }}) } ) "
        ).scale(0.8).next_to(self.trans_eq_3, DOWN)
        self.trans_eq_4[0].set_color(self.color_map["E_trans"])
        self.trans_eq_4[2].set_color(self.color_map["E_in"])
        self.trans_eq_4[6].set_color(self.color_map["lc"])
        self.trans_eq_4[8].set_color(self.color_map["ld"])
        self.trans_eq_4[12].set_color(self.color_map["lc"])
        self.trans_eq_4[14].set_color(self.color_map["ld"])

        self.trans_eq_5 = MathTex(
            r"{{ E_\mathrm{trans} }} = { {{ E_\mathrm{in} }} \over 2}{{ }}e^{i 2 k {{ \ell_c }}}{{ ( }}-e^{ i 2 k {{ \ell_d }} }{{ + }}e^{-i 2 k {{ \ell_d }} } ) "
        ).scale(0.8).next_to(self.trans_eq_4, DOWN)
        self.trans_eq_5[0].set_color(self.color_map["E_trans"])
        self.trans_eq_5[2].set_color(self.color_map["E_in"])
        self.trans_eq_5[6].set_color(self.color_map["lc"])
        self.trans_eq_5[10].set_color(self.color_map["ld"])
        self.trans_eq_5[14].set_color(self.color_map["ld"])

        self.trans_eq_7 = MathTex(
            r"{ {{ E_\mathrm{trans} }} \over {{ E_\mathrm{in} }} }{{ = }}-i e^{ i 2 k {{ \ell_c }} }{{ }}\sin{( 2 k {{ \ell_d }} )}"
        ).scale(0.8).next_to(self.trans_eq_5, DOWN)
        self.trans_eq_7[1].set_color(self.color_map["E_trans"])
        self.trans_eq_7[3].set_color(self.color_map["E_in"])
        self.trans_eq_7[7].set_color(self.color_map["lc"])
        self.trans_eq_7[11].set_color(self.color_map["ld"])

        # Reflection matrix
        self.reflection_matrix_tex = MathTex(
            r"\begin{bmatrix} "+\
                r"-r    & \tau \\"+\
                r" \tau & r"+\
            r"\end{bmatrix}"
        ).scale(0.7).shift(3*UP + 2*RIGHT)

        # Signs
        self.plus_sign = MathTex(
            r" + r ",
            color=BLUE
        ).scale(0.7).next_to(self.bs, DOWN+LEFT).shift(0.7*UP + 0.1*RIGHT)
        self.minus_sign = MathTex(
            r" - r ",
            color=BLUE
        ).scale(0.7).next_to(self.bs, DOWN+LEFT).shift(0.7*RIGHT + 0.1*UP)

        # Assumptions
        self.end_mirror_assump = MathTex(
            r"{{ r_\mathrm{end} }} = 1"
        ).scale(0.7)
        self.end_mirror_assump[0].set_color(self.color_map["mirror"])

        self.bs_refl_tex = MathTex(
            r"|{{ r }}|^2 = | {{ \tau }}|^2 = {1 \over 2}"
        ).scale(0.7).next_to(self.reflection_matrix_tex, DOWN)
        self.bs_refl_tex[1].set_color(self.color_map["mirror"])
        self.bs_refl_tex[3].set_color(self.color_map["mirror"])

        # Motion definitions
        self.common_motion_tex = Tex(
            "Common Motion",
            color=self.color_map["lc"]
        ).move_to(2*UP + 4*RIGHT)
        self.common_motion_mathtex = MathTex(
            r"{{ \ell_c }} = { \ell_x + \ell_y \over 2 }",
        ).next_to(self.common_motion_tex, DOWN)
        self.common_motion_mathtex[0].set_color(self.color_map["lc"])

        self.differential_motion_tex = Tex(
            "Differential Motion",
            color=self.color_map["ld"]
        ).move_to(2*UP + 4*RIGHT)
        self.differential_motion_mathtex = MathTex(
            r"{{ \ell_d }} = { \ell_x - \ell_y \over 2 }",
            color=self.color_map["ld"]
        ).next_to(self.differential_motion_tex, DOWN)

        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end, mob.y_start, mob.y_end, 
                mob.stroke_width, mob.rotate_angle, mob.rotate_point
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.field1
            bmob = mob.field2
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end, fmob.y_start, fmob.y_end, 
                fmob.stroke_width, fmob.rotate_angle, fmob.rotate_point
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end, bmob.y_start, bmob.y_end, 
                bmob.stroke_width, bmob.rotate_angle, bmob.rotate_point
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, 
                color=mob.color, reverse=mob.reverse, y_axis=mob.y_axis, stroke_width=mob.stroke_width, sign_flip=mob.sign_flip
            )
            mob.become(temp_mob)
            return 

        def y_arm_length_updater(mob):
            """Because my y-arms are so wonky, I have to manipulate the ValueTrackers in a weird way :(
            """
            x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_main_beam, y_mirror_length.get_value())

            x_start_y_arm_1.set_value(x1)
            x_end_y_arm_1.set_value(x2)
            y_start_y_arm_1.set_value(y1)
            y_end_y_arm_1.set_value(y2)

            x1, x2, y1, y2, theta = unrotate(0.0, 0.0, y_mirror_length.get_value(), 0.0)

            x_start_y_arm_2.set_value(x1)
            x_end_y_arm_2.set_value(x2)
            y_start_y_arm_2.set_value(y1)
            y_end_y_arm_2.set_value(y2)

            return


        self.input_beam.add_updater(wave_updater)
        self.y_arm_1_beam.add_updater(wave_updater)
        self.y_arm_2_beam.add_updater(wave_updater)
        self.x_arm_1_beam.add_updater(wave_updater)
        self.x_arm_2_beam.add_updater(wave_updater)
        self.refl_y_beam.add_updater(wave_updater)
        self.refl_x_beam.add_updater(wave_updater)
        self.trans_y_beam.add_updater(wave_updater)
        self.trans_x_beam.add_updater(wave_updater)

        self.y_arm_beam.add_updater(standing_updater)
        self.x_arm_beam.add_updater(standing_updater)
        self.refl_beam.add_updater(standing_updater)
        self.trans_beam.add_updater(standing_updater)

        y_mirror_length.add_updater(y_arm_length_updater)

        ### Scene
        # Add ValueTrackers
        self.add(bs_refl)
        self.add(bs_trans)

        self.add(x_mirror_length)
        self.add(y_mirror_length)

        self.add(amp_y_arm_1_scaler)
        self.add(amp_y_arm_2_scaler)
        self.add(amp_x_arm_1_scaler)
        self.add(amp_x_arm_2_scaler)
        self.add(amp_refl_y_scaler)
        self.add(amp_refl_x_scaler)
        self.add(amp_trans_y_scaler)
        self.add(amp_trans_x_scaler)

        self.add(phase_y_arm_1_offset)
        self.add(phase_y_arm_2_offset)
        self.add(phase_x_arm_1_offset)
        self.add(phase_x_arm_2_offset)
        self.add(phase_refl_x_offset)
        self.add(phase_refl_y_offset)
        self.add(phase_trans_x_offset)
        self.add(phase_trans_y_offset)

        self.add(x_start_y_arm_1)
        self.add(x_end_y_arm_1)
        self.add(y_start_y_arm_1)
        self.add(y_end_y_arm_1)

        self.add(x_start_y_arm_2)
        self.add(x_end_y_arm_2)
        self.add(y_start_y_arm_2)
        self.add(y_end_y_arm_2)

        # Start with the end of michelson_derivation_2.py, just the two equations
        self.add(self.refl_eq_7)
        self.add(self.trans_eq_7)

        # Laser
        self.play(
            Create(self.laser),
            Create(self.bs),
            Create(self.x_mirror),
            Create(self.y_mirror),
            Create(self.photodetector),
        )

        # Beams
        self.play(
            Create(self.input_beam)
        )
        self.play(
            Create(self.x_arm_1_beam),
            Create(self.y_arm_1_beam)
        )
        self.play(
            Create(self.x_arm_2_beam),
            Create(self.y_arm_2_beam)
        )
        self.play(
            Create(self.refl_beam),
            Create(self.trans_beam)
        )

        # Lengths
        self.play(
            Create(self.x_arm_length_group),
            Create(self.y_arm_length_group)
        )
        self.wait(2)

        # Move the arms in common
        self.play(
            Write(self.common_motion_tex),
        )
        self.play(
            Write(self.common_motion_mathtex)
        )
        self.wait(2)

        self.play(
            x_mirror_length.animate.set_value(3.5),
            y_mirror_length.animate.set_value(3.5),
            run_time=2
        )
        self.wait(1)
        self.play(
            x_mirror_length.animate.set_value(3.3),
            y_mirror_length.animate.set_value(3.3)
        )
        self.wait(1)
        self.play(
            x_mirror_length.animate.set_value(3.0),
            y_mirror_length.animate.set_value(3.0),
            run_time=2
        )
        self.wait(1)

        # Move the arms differentially
        self.play(
            TransformMatchingTex(self.common_motion_tex, self.differential_motion_tex),
        )
        self.play(
            TransformMatchingTex(self.common_motion_mathtex, self.differential_motion_mathtex)
        )
        self.wait(2)

        self.play(
            x_mirror_length.animate.set_value(3.25),
            y_mirror_length.animate.set_value(2.75),
            run_time=2
        )
        self.wait(1)
        self.play(
            x_mirror_length.animate.set_value(3.5),
            y_mirror_length.animate.set_value(2.5),
            run_time=2
        )
        self.wait(1)
        self.play(
            x_mirror_length.animate.set_value(3.0),
            y_mirror_length.animate.set_value(3.0),
            run_time=2
        )
        self.wait(1)

