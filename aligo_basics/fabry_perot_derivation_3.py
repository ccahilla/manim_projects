"""fabry_perot_derivation_3.py
Starts with circulating power derivation, 
redraw the FP,
derive the reflected signal,
and takes the derivative wrt L.
"""

from manim import *

def get_cav_refl(phi, r1, r2):
    """Get the Fabry-Perot cavity amplitude reflection.
    phi is single-pass phase of the light.
    """
    return (-r1 + r2 * np.exp(2j * phi))/(1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_end_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_buildup_toward_input_mirror(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    return t1 * r2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))

def get_cav_trans(phi, r1, r2):
    """Get the cavity buildup going toward the end mirror.
    phi is single-pass phase of the light.
    """
    t1 = np.sqrt(1 - r1**2)
    t2 = np.sqrt(1 - r2**2)
    return t1 * t2 * np.exp(1j * phi) / (1 - r1 * r2 * np.exp(2j * phi))
    """Make a two-mirror cavity, with five propogating E-fields
    1) Input beam
    2) Refl beam
    3) Cavity beam heading to end mirror
    4) Cavity beam heading back to input mirror
    5) Transmitted beam
    """

class FabryPerotDerivation3(Scene):
    def get_phase(self, k=1, x=0, freq=1, time=0, phase=0):
        return k * x - 2 * PI * freq * time + phase
    def get_sine_wave(self, amp=1, k=1, x=0, freq=1, time=0, phase=0):
        return  amp * np.sin(k * x - 2 * PI * freq * time + phase)

    def rotate(self, x1, y1, theta, origin=np.array([0,0,0])):
        """Return x2 and y2 being rotated about the origin by theta.
        """
        x0 = origin[0]
        y0 = origin[1]
        x2 = (x1 - x0) * np.cos(theta) - (y1 - y0) * np.sin(theta)
        y2 = (x1 - x0) * np.sin(theta) + (y1 - y0) * np.cos(theta)
        return x2, y2

    def show_axis(self):
        x_start = np.array([-5,0,0])
        x_end = np.array([5,0,0])

        y_start = np.array([0,-3,0])
        y_end = np.array([0,3,0])

        x_axis = Line(x_start, x_end)
        y_axis = Line(y_start, y_end)

        # self.add(x_axis, y_axis)

        self.origin_point = np.array([0,0,0])

        return

    def make_electric_field(
        self, 
        amp=ValueTracker(1), 
        k=ValueTracker(1), 
        freq=ValueTracker(1), 
        time=0, 
        phase=ValueTracker(0), 
        start=np.array([-5,0,0]), 
        end=np.array([5,0,0]), 
        number_of_lines=50, 
        color=RED,
        amp_scaler=ValueTracker(1),
        k_scaler=1,
        freq_scaler=1,
        phase_offset=ValueTracker(0.0),
        x_start=None,
        x_end=None,
    ):
        """Makes an electric field VGroup of Lines.
        Electric field is defined by amp, k, freq, time, and phase ValueTrackers.
        start and end are 3D vectors defining the beginning and end of the wave.
        number_of_lines defines how many Line segments the wave is broken up into.
        x_start and x_end should either be None if we don't want to move the sine wave
        starting points, or a ValueTracker if we do.
        Returns the VGroup.
        """
        # Define the VGroup of Lines which make up the E-field, and store some ValueTrackers
        beam = VGroup()
        beam.amp = amp
        beam.k = k
        beam.freq = freq
        beam.time = time
        beam.phase = phase

        beam.x_start = x_start
        beam.x_end = x_end

        beam.amp_scaler = amp_scaler
        beam.k_scaler = k_scaler
        beam.freq_scaler = freq_scaler
        beam.phase_offset = phase_offset

        amp_float = amp_scaler.get_value() * amp.get_value()
        k_float = k_scaler * k.get_value()
        freq_float = freq_scaler * freq.get_value()
        phase_float = phase_offset.get_value() + phase.get_value()

        # Check if ValueTrackers exist
        if x_start:
            start[0] = x_start.get_value()
        if x_end:
            end[0] = x_end.get_value()

        # Get the start and end vector info.
        # This function will first calculate the wave along the x axis,
        # then rotate it appropriately.
        span = end - start
        origin = (start + end) / 2
        total_length = np.sqrt(np.sum(span**2))
        norm = span / total_length
        theta = np.arctan2(norm[1], norm[0])

        x_diff = total_length / number_of_lines

        phase_output = self.get_phase(k=k_float, x=total_length, freq=freq_float, time=time, phase=0)

        # Store some additional info
        beam.phase_output = phase_output
        beam.start = start
        beam.end = end
        beam.origin = origin
        beam.theta = theta
        beam.number_of_lines = number_of_lines
        beam.color = color

        for ii in range(number_of_lines):
            # Calculate wave in 1D
            temp_x_start = ii * x_diff
            temp_x_end = (ii + 1) * x_diff

            temp_y_start = self.get_sine_wave(amp_float, k_float, temp_x_start, freq_float, time, phase_float)
            temp_y_end = self.get_sine_wave(amp_float, k_float, temp_x_end, freq_float, time, phase_float)

            # Translate to origin = (0, 0)
            temp_x_start -= total_length/2
            temp_x_end -= total_length/2
            # temp_y_start += origin[1]
            # temp_y_end += origin[1]

            # Rotate about origin
            temp_x_start, temp_y_start = self.rotate(temp_x_start, temp_y_start, theta)
            temp_x_end, temp_y_end = self.rotate(temp_x_end, temp_y_end, theta)

            # Translate to sine wave position beam.origin
            temp_x_start += origin[0]
            temp_x_end += origin[0]
            temp_y_start += origin[1]
            temp_y_end += origin[1]

            # Define the Line
            temp_start = np.array([temp_x_start, temp_y_start, 0])
            temp_end = np.array([temp_x_end, temp_y_end, 0])

            temp_line = Line(temp_start, temp_end, color=color)
            beam.add(temp_line)

        return beam

    def sum_electric_fields(self, field1, field2, color=YELLOW, reverse=True):
        """Add together two VGroups of electric fields
        """
        if field1.number_of_lines != field2.number_of_lines:
            print()
            print("Cannot sum two fields of different lengths")
            return
        
        beam = VGroup()
        beam.color = color
        for ii in range(field1.number_of_lines):
            if reverse:
                jj = (field1.number_of_lines - 1) - ii
            else:
                jj = ii
        
            line1 = field1[ii]
            line2 = field2[jj]

            if reverse:
                new_line_start = line1.start
                new_line_end = line1.end
                new_line_start[1] = line1.start[1] + line2.end[1]
                new_line_end[1] = line1.end[1] + line2.start[1]
            else:
                new_line_start = line1.start 
                new_line_end = line1.end 
                new_line_start[1] = line1.start[1] + line2.start[1]
                new_line_end[1] = line1.end[1] + line2.end[1]

            new_line = Line(new_line_start, new_line_end, color=color)
            beam.add(new_line)

        return beam

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.25
        number_of_lines = 150

        self.color_map = {
            "in" : RED,
            "refl" : ORANGE,
            "cav east" : PURPLE,
            "cav west" : PINK,
            "trans" : YELLOW,
            "mirror" : BLUE,
        }

        x_start_input = ValueTracker(-7.0)
        x_input_mirror = ValueTracker(-3.0)
        x_end_mirror = ValueTracker(3.0)
        x_end_input = ValueTracker(7.0)

        # Mirrors
        self.mirror_width = 0.25
        self.input_mirror_refl = ValueTracker(np.sqrt(0.5))
        self.end_mirror_refl = ValueTracker(np.sqrt(0.5))

        self.input_mirror = Rectangle(
            height=3.0, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.input_mirror_refl.get_value()**2, color=BLUE
        ).shift((x_input_mirror.get_value() - self.mirror_width/2) * RIGHT)

        self.end_mirror = Rectangle(
            height=3.0, width=self.mirror_width, fill_color=BLUE, fill_opacity=self.end_mirror_refl.get_value()**2, color=BLUE
        ).shift((x_end_mirror.get_value() + self.mirror_width/2) * RIGHT)


        # Input Beam
        amp_input = ValueTracker(0.75)
        k_input = ValueTracker(PI / 2)
        freq_input = ValueTracker(1)
        time_input = 0.0
        phase_input = ValueTracker(0.0)
        
        start_input = np.array([-7.0, 0.0, 0.0])
        end_input = np.array([-3.0, 0.0, 0.0])

        # Calculate the cavity params
        length_input = x_end_mirror.get_value() - x_input_mirror.get_value()
        phi_input = k_input.get_value() * (end_input[0] - start_input[0])

        phi = k_input.get_value() * length_input
        r1 = self.input_mirror_refl.get_value()
        r2 = self.end_mirror_refl.get_value()

        phase_input.set_value( -1 * phi_input )

        self.input_beam = self.make_electric_field(
            amp_input, 
            k_input, 
            freq_input, 
            time_input, 
            phase_input, 
            start_input, 
            end_input, 
            number_of_lines,
            color=self.color_map["in"],
            x_start=x_start_input,
            x_end=x_input_mirror,
        )

        # Refl Beam
        amp_refl = amp_input
        k_refl = k_input
        freq_refl = freq_input
        time_refl = 0.0
        phase_refl = phase_input
        x_start_refl = x_input_mirror
        x_end_refl = x_start_input

        start_refl = np.array([-3.0, 0.0, 0.0])
        end_refl = np.array([-7.0, 0.0, 0.0])

        cav_refl = get_cav_refl(phi, r1, r2)

        amp_refl_scaler = ValueTracker(np.abs(cav_refl))
        k_refl_scaler = 1
        freq_refl_scaler = 1
        phase_refl_offset = ValueTracker(np.angle(cav_refl))

        self.refl_beam = self.make_electric_field(
            amp_refl, 
            k_refl, 
            freq_refl, 
            time_refl, 
            phase_refl, 
            start_refl, 
            end_refl, 
            number_of_lines,
            color=self.color_map["refl"],
            amp_scaler=amp_refl_scaler,
            k_scaler=k_refl_scaler,
            freq_scaler=freq_refl_scaler,
            phase_offset=phase_refl_offset,
            x_start=x_start_refl,
            x_end=x_end_refl
        )

        # Cavity Toward End Mirror Beam
        amp_cav_east = amp_input
        k_cav_east = k_input
        freq_cav_east = freq_input
        time_cav_east = 0.0
        phase_cav_east = phase_input

        x_start_cav_east = x_input_mirror
        x_end_cav_east = x_end_mirror

        start_cav_east = np.array([-3.0, 0.0, 0.0])
        end_cav_east = np.array([3.0, 0.0, 0.0])

        cav_east = get_cav_buildup_toward_end_mirror(phi, r1, r2)

        amp_cav_east_scaler = ValueTracker(np.abs(cav_east))
        k_cav_east_scaler = 1
        freq_cav_east_scaler = 1
        phase_cav_east_offset = ValueTracker(np.angle(cav_east))

        self.cav_east_beam = self.make_electric_field(
            amp_cav_east, 
            k_cav_east, 
            freq_cav_east, 
            time_cav_east, 
            phase_cav_east, 
            start_cav_east, 
            end_cav_east, 
            number_of_lines,
            color=self.color_map["cav east"],
            amp_scaler=amp_cav_east_scaler,
            k_scaler=k_cav_east_scaler,
            freq_scaler=freq_cav_east_scaler,
            phase_offset=phase_cav_east_offset,
            x_start=x_start_cav_east,
            x_end=x_end_cav_east
        )

        # Cavity Toward Input Mirror Beam
        amp_cav_west = amp_input
        k_cav_west = k_input
        freq_cav_west = freq_input
        time_cav_west = 0.0
        phase_cav_west = phase_input

        x_start_cav_west = x_end_mirror
        x_end_cav_west = x_input_mirror

        start_cav_west = np.array([3.0, 0.0, 0.0])
        end_cav_west = np.array([-3.0, 0.0, 0.0])

        cav_west = get_cav_buildup_toward_input_mirror(phi, r1, r2)

        amp_cav_west_scaler = ValueTracker(np.abs(cav_west))
        k_cav_west_scaler = 1
        freq_cav_west_scaler = 1
        phase_cav_west_offset = ValueTracker(np.angle(cav_west))

        self.cav_west_beam = self.make_electric_field(
            amp_cav_west, 
            k_cav_west, 
            freq_cav_west, 
            time_cav_west, 
            phase_cav_west, 
            start_cav_west, 
            end_cav_west, 
            number_of_lines,
            color=self.color_map["cav west"],
            amp_scaler=amp_cav_west_scaler,
            k_scaler=k_cav_west_scaler,
            freq_scaler=freq_cav_west_scaler,
            phase_offset=phase_cav_west_offset,
            x_start=x_start_cav_west,
            x_end=x_end_cav_west
        )

        # Transmitted Beam
        amp_trans = amp_input
        k_trans = k_input
        freq_trans = freq_input
        time_trans = 0.0
        phase_trans = phase_input
        x_start_trans = x_end_mirror
        x_end_trans = x_end_input

        start_trans = np.array([3.0, 0.0, 0.0])
        end_trans = np.array([7.0, 0.0, 0.0])

        cav_trans = get_cav_trans(phi, r1, r2)

        amp_trans_scaler = ValueTracker(np.abs(cav_trans))
        k_trans_scaler = 1
        freq_trans_scaler = 1
        phase_trans_offset = ValueTracker(np.angle(cav_trans))

        self.trans_beam = self.make_electric_field(
            amp_trans, 
            k_trans, 
            freq_trans, 
            time_trans, 
            phase_trans, 
            start_trans, 
            end_trans, 
            number_of_lines,
            color=self.color_map["trans"],
            amp_scaler=amp_trans_scaler,
            k_scaler=k_trans_scaler,
            freq_scaler=freq_trans_scaler,
            phase_offset=phase_trans_offset,
            x_start=x_start_trans,
            x_end=x_end_trans
        )


        ## Tex
        # Electric field equations
        self.input_tex    = MathTex(r"E_\mathrm{in}", color=self.color_map["in"]).move_to(2*UP + 5*LEFT)
        self.refl_tex     = MathTex(r"E_\mathrm{refl}", color=self.color_map["refl"]).move_to(2*DOWN + 5*LEFT)
        self.cav_east_tex = MathTex(r"E_\mathrm{cav\,east}", color=self.color_map["cav east"]).move_to(2*UP)
        self.cav_west_tex = MathTex(r"E_\mathrm{cav\,west}", color=self.color_map["cav west"]).move_to(2*DOWN)
        self.trans_tex    = MathTex(r"E_\mathrm{trans}", color=self.color_map["trans"]).move_to(2*UP + 5*RIGHT)

        start_equation_height = 2 * UP
        start_refl_equation_height = 3 * DOWN
        final_equation_height = 3 * UP
        
        self.electric_field_sum_6 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = { {{ \tau_i }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } } {{ E_\mathrm{in} }}"
        )
        self.electric_field_sum_6[0].set_color(PURPLE)
        self.electric_field_sum_6[2].set_color(BLUE)
        self.electric_field_sum_6[4].set_color(BLUE)
        self.electric_field_sum_6[5].set_color(BLUE)
        self.electric_field_sum_6[7].set_color(RED)
        self.electric_field_sum_6.move_to(start_equation_height)

        self.electric_field_refl_1 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ - r_i }}{{ E_\mathrm{in} }} + {{ \tau_i }}{{ r_e }}{{ e^{ i k (2 L) } }}{{ E_\mathrm{cav\,east} }}"
        ).move_to(start_refl_equation_height)
        self.electric_field_refl_1[0].set_color(self.color_map["refl"])
        self.electric_field_refl_1[2].set_color(self.color_map["mirror"])
        self.electric_field_refl_1[3].set_color(self.color_map["in"])
        self.electric_field_refl_1[5].set_color(self.color_map["mirror"])
        self.electric_field_refl_1[6].set_color(self.color_map["mirror"])
        self.electric_field_refl_1[8].set_color(self.color_map["cav east"])

        self.electric_field_refl_2 = MathTex(
            r"{{ E_\mathrm{refl} }} = {{ - r_i }}{{ E_\mathrm{in} }} + { {{ \tau_i }}^2 {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }{{ E_\mathrm{in} }}"
        ).move_to(self.electric_field_refl_1)
        self.electric_field_refl_2[0].set_color(self.color_map["refl"])
        self.electric_field_refl_2[2].set_color(self.color_map["mirror"])
        self.electric_field_refl_2[3].set_color(self.color_map["in"])
        self.electric_field_refl_2[5].set_color(self.color_map["mirror"])
        self.electric_field_refl_2[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_2[10].set_color(self.color_map["mirror"])
        self.electric_field_refl_2[11].set_color(self.color_map["mirror"])
        self.electric_field_refl_2[13].set_color(self.color_map["in"])

        self.electric_field_refl_3 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ - r_i }} + ({{ r_i }}^2 + {{ \tau_i }}^2) {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }{{ E_\mathrm{in} }}"
        ).move_to(final_equation_height)
        self.electric_field_refl_3[0].set_color(self.color_map["refl"])
        self.electric_field_refl_3[2].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[4].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[6].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[8].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[11].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[12].set_color(self.color_map["mirror"])
        self.electric_field_refl_3[14].set_color(self.color_map["in"])

        self.electric_field_refl_4 = MathTex(
            r"{{ E_\mathrm{refl} }} = { {{ - r_i }} + {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }{{ E_\mathrm{in} }}"
        ).move_to(final_equation_height)
        self.electric_field_refl_4[0].set_color(self.color_map["refl"])
        self.electric_field_refl_4[2].set_color(self.color_map["mirror"])
        self.electric_field_refl_4[4].set_color(self.color_map["mirror"])
        self.electric_field_refl_4[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_4[8].set_color(self.color_map["mirror"])
        self.electric_field_refl_4[10].set_color(self.color_map["in"])

        self.electric_field_refl_5 = MathTex(
            r"{ {{ E_\mathrm{refl} }} \over {{ E_\mathrm{in} }} }{{ = }}{ {{ - r_i }} + {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }"
        ).move_to(final_equation_height)
        self.electric_field_refl_5[1].set_color(self.color_map["refl"])
        self.electric_field_refl_5[3].set_color(self.color_map["in"])
        self.electric_field_refl_5[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_5[9].set_color(self.color_map["mirror"])
        self.electric_field_refl_5[12].set_color(self.color_map["mirror"])
        self.electric_field_refl_5[13].set_color(self.color_map["mirror"])
        
        self.cavity_refl_mathtex = MathTex(
            r"{{ r_\mathrm{cav} }} = "
        ).next_to(self.electric_field_refl_5, LEFT)
        self.cavity_refl_mathtex[0].set_color(ORANGE)

        self.cavity_refl_tex = Tex(
            "Complex\\\\cavity\\\\reflection",
            color=ORANGE
        ).scale(0.7).next_to(self.cavity_refl_mathtex, LEFT)

        # Length as Decimal Number
        self.cavity_length = ValueTracker(x_end_mirror.get_value() - x_input_mirror.get_value())
        self.length_value_tex = Tex(r"$L = $")
        self.length_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=2)
            .set_value(self.cavity_length.get_value())
            .next_to(self.length_value_tex, RIGHT, buff=0.1)
        )
        self.length_value_group = always_redraw(
            lambda: VGroup(self.length_value_tex, self.length_value)
            .move_to( np.array([(self.input_mirror.get_x() + self.end_mirror.get_x())/2.0, 2.0, 0.0]) )
        )

        # Mirrors Tex
        self.input_mirror_refl_value_tex = MathTex(r"r_i^2 = ", color=BLUE)
        self.input_mirror_refl_value = DecimalNumber(
            num_decimal_places=2, color=BLUE
        ).next_to(self.input_mirror_refl_value_tex, RIGHT)
        self.input_mirror_refl_value.add_updater(
            lambda mob: mob.set_value(self.input_mirror_refl.get_value()**2)
        )
        self.input_mirror_tex_group = VGroup(
            self.input_mirror_refl_value_tex, self.input_mirror_refl_value
        ).move_to(3*UP + 4.5*RIGHT)
        

        self.end_mirror_refl_value_tex = MathTex(r"r_e^2 = ", color=BLUE)
        self.end_mirror_refl_value = DecimalNumber(
            num_decimal_places=2, color=BLUE
        ).next_to(self.end_mirror_refl_value_tex, RIGHT)
        self.end_mirror_refl_value.add_updater(
            lambda mob: mob.set_value(self.end_mirror_refl.get_value()**2)
        )
        self.end_mirror_tex_group = VGroup(
            self.end_mirror_refl_value_tex, self.end_mirror_refl_value
        ).next_to(self.input_mirror_tex_group, DOWN)    

        # Cavity gain Tex
        self.cavity_gain_value_tex = MathTex(
            r"{{ g }} = { {{ E_\mathrm{cav\,east} }} \over {{ E_\mathrm{in} }} } = "
        )
        self.cavity_gain_value_tex[0].set_color(YELLOW)
        self.cavity_gain_value_tex[2].set_color(PURPLE)
        self.cavity_gain_value_tex[4].set_color(RED)

        self.cavity_gain_value = DecimalNumber(
            num_decimal_places=1, color=YELLOW
        ).next_to(self.cavity_gain_value_tex, RIGHT)
        self.cavity_gain_value.add_updater(
            lambda mob: mob.set_value(amp_cav_east_scaler.get_value())
        )
        self.cavity_gain_value_group = VGroup(
            self.cavity_gain_value_tex, self.cavity_gain_value
        ).next_to(self.end_mirror_tex_group, DOWN).shift(4*DOWN)

        self.cavity_gain_tex = Tex(
            "Cavity gain:",
            color=YELLOW
        ).next_to(self.cavity_gain_value_group, LEFT)

        # Coupling tex
        self.critically_coupled_tex = Tex(
            "Critically coupled",
            color=BLUE
        )
        self.overcoupled_tex = Tex(
            "Overcoupled",
            color=BLUE
        )

        # Phase Tex
        self.phase_value_tex = MathTex(
            r"e^{ i k (2 L) }",
            color=GREEN
        ).move_to(2.5*DOWN + LEFT)
        # self.phase_value = DecimalNumber(
        #     num_decimal_places=2, color=BLUE
        # ).next_to(self.input_mirror_refl_value_tex, RIGHT)
        # self.phase_value.add_updater(
        #     lambda mob: mob.set_value(self.phase.get_value())
        # )
        # self.end_mirror_tex_group = VGroup(
        #     self.phase_value_tex, self.phase_value
        # ).next_to(self.end_mirror_tex_group, DOWN)    

        # Phase graph
        self.cavity_length = ValueTracker(x_end_mirror.get_value() - x_input_mirror.get_value())
        self.phase = ValueTracker(2 * k_input.get_value() * self.cavity_length.get_value() % (2 * PI))

        # graph_origin = np.array([-4.0, 2.5, 0.0])
        graph_origin = self.phase_value_tex.get_center() + 2.0 * RIGHT
        graph_length = 1
        self.x_axis = Line(
            start=(graph_origin[0] - graph_length)*RIGHT + graph_origin[1]*UP,
            end=(graph_origin[0] + graph_length)*RIGHT + graph_origin[1]*UP,
            stroke_width=2
        )
        self.y_axis = Line(
            start=graph_origin[0]*RIGHT + (graph_origin[1] - graph_length)*UP,
            end=graph_origin[0]*RIGHT + (graph_origin[1] + graph_length)*UP,
            stroke_width=2
        )
        self.phase_arrow_moving = Arrow(
            start=graph_origin,
            end=graph_origin + RIGHT,
            color=GREEN,
            buff=0.0
        )
        self.phase_arrow_ref = self.phase_arrow_moving.copy()
        self.phase_arrow_moving.add_updater(
            lambda mob: mob.become(self.phase_arrow_ref.copy()).rotate(self.phase.get_value(), about_point=graph_origin)
        )

        self.phase_graph_group = VGroup(
            self.phase_value_tex,
            self.x_axis, 
            self.y_axis, 
            self.phase_arrow_moving
        )

        # Refl phase graph
        # refl_graph_origin = np.array([-4.0, 2.5, 0.0])
        refl_graph_origin = 2.5*DOWN + 4.5*LEFT
        refl_graph_length = 1.4
        self.refl_x_axis = Line(
            start=(refl_graph_origin[0] - refl_graph_length)*RIGHT + refl_graph_origin[1]*UP,
            end=(refl_graph_origin[0] + refl_graph_length)*RIGHT + refl_graph_origin[1]*UP,
            stroke_width=2
        )
        self.refl_y_axis = Line(
            start=refl_graph_origin[0]*RIGHT + (refl_graph_origin[1] - refl_graph_length)*UP,
            end=refl_graph_origin[0]*RIGHT + (refl_graph_origin[1] + refl_graph_length)*UP,
            stroke_width=2
        )
        self.refl_phase_arrow_moving = Line(
            start=refl_graph_origin,
            end=refl_graph_origin + refl_graph_length*RIGHT,
            color=ORANGE
        )
        self.refl_phase_arrow_ref = self.refl_phase_arrow_moving.copy()
        self.refl_phase_arrow_moving.add_updater(
            lambda mob: mob.become(
                Line(
                    start=refl_graph_origin,
                    end=refl_graph_origin + refl_graph_length * amp_refl_scaler.get_value() * RIGHT,
                    color=ORANGE
                )
            ).rotate(phase_refl_offset.get_value(), about_point=refl_graph_origin)
        )

        def make_refl_ring(r1, r2, origin, scaler=1.0, color=ORANGE, stroke_width=2):
            """Make VGroup ring of Lines representing the complex phase on reflection from a Fabry Perot.
            """
            refl_ring = VGroup()
            phis_plus = np.geomspace(1e-5, PI/2, 100)
            phis = np.append(-1*phis_plus, phis_plus)

            r_cavs = np.array([])
            for phi in phis:
                temp_r_cav = get_cav_refl(phi, r1, r2)
                r_cavs = np.append(temp_r_cav, r_cavs)

            for ii, r_cav in enumerate(r_cavs):
                if ii == len(r_cavs) - 1:
                    break
                start = scaler * np.array([ np.real(r_cavs[ii]),   np.imag(r_cavs[ii]), 0.0]) + origin
                end   = scaler * np.array([ np.real(r_cavs[ii+1]), np.imag(r_cavs[ii+1]), 0.0]) + origin

                temp_line = Line(start=start, end=end, color=color, stroke_width=stroke_width)

                refl_ring.add(temp_line)

            return refl_ring

        self.refl_ring = make_refl_ring(
            self.input_mirror_refl.get_value(),
            self.end_mirror_refl.get_value(),
            refl_graph_origin,
            scaler=refl_graph_length
        )

        self.cavity_refl_mathtex_no_equals = MathTex(
            r"{{ r_\mathrm{cav} }}"
        ).next_to(self.refl_x_axis, RIGHT)
        self.cavity_refl_mathtex_no_equals[0].set_color(ORANGE)

        self.refl_phase_graph_group = VGroup(
            self.cavity_refl_mathtex_no_equals,
            self.refl_ring, 
            self.refl_x_axis, 
            self.refl_y_axis, 
            self.refl_phase_arrow_moving
        )


        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def wave_updater(mob):
            temp_mob = self.make_electric_field(
                mob.amp, mob.k, mob.freq, self.timer, mob.phase, mob.start, mob.end, mob.number_of_lines, mob.color, 
                mob.amp_scaler, mob.k_scaler, mob.freq_scaler, mob.phase_offset, mob.x_start, mob.x_end
            )
            mob.become(temp_mob)
            return 

        def standing_updater(mob):
            fmob = mob.forward_beam
            bmob = mob.backward_beam
            temp_fmob = self.make_electric_field(
                fmob.amp, fmob.k, fmob.freq, self.timer, fmob.phase, fmob.start, fmob.end, fmob.number_of_lines, fmob.color, 
                fmob.amp_scaler, fmob.k_scaler, fmob.freq_scaler, fmob.phase_offset, fmob.x_start, fmob.x_end
            )
            temp_bmob = self.make_electric_field(
                bmob.amp, bmob.k, bmob.freq, self.timer, bmob.phase, bmob.start, bmob.end, bmob.number_of_lines, bmob.color, 
                bmob.amp_scaler, bmob.k_scaler, bmob.freq_scaler, bmob.phase_offset, bmob.x_start, bmob.x_end
            )
            temp_mob = self.sum_electric_fields(temp_fmob, temp_bmob, color=mob.color)
            mob.become(temp_mob)
            return 


        #####   Scene   #####
        # ValueTrackers
        self.add(k_input)
        self.add(x_input_mirror)
        self.add(x_end_mirror)
        self.add(self.cavity_length)
        self.add(self.phase)

        # Start with cav east electric field
        self.add(self.electric_field_sum_6)

        self.wait(2)

        self.add(k_input)
        # self.add(self.cavity_length)
        self.add(self.input_mirror_refl)
        self.add(self.end_mirror_refl)
        # self.add(self.cav_gain)

        # Set the scene and play it
        # self.show_axis()

        self.play(
            GrowFromCenter(self.input_mirror),
            GrowFromCenter(self.end_mirror)
        )
        self.wait(1)

        self.play(
            Create(self.input_beam),
            Create(self.refl_beam),
            Create(self.cav_east_beam),
            Create(self.cav_west_beam),
            Create(self.trans_beam),
        )
        self.wait(1)
        self.play(
            self.electric_field_sum_6.animate.move_to(final_equation_height)
        )
        self.wait(1)
        self.play(
            # Write(self.length_value_group),
            Write(self.input_tex),
            Write(self.refl_tex),
            Write(self.cav_east_tex),
            # Write(self.cav_west_tex),
            # Write(self.trans_tex)
        )
        self.wait(2)

        # Start the refl derivation
        self.play(
            Write(self.electric_field_refl_1),
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.electric_field_sum_6, self.electric_field_refl_2),
            TransformMatchingTex(self.electric_field_refl_1, self.electric_field_refl_2),
            Unwrite(self.cav_east_tex),
        )
        self.wait(2)
        self.play(
            self.electric_field_refl_2.animate.move_to(final_equation_height)
        )
        self.wait(1)
        self.play(
            TransformMatchingTex(self.electric_field_refl_2, self.electric_field_refl_3),
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.electric_field_refl_3, self.electric_field_refl_4),
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.electric_field_refl_4, self.electric_field_refl_5)
        )
        self.wait(2)
        self.play(
            Write(self.cavity_refl_tex),
            Write(self.cavity_refl_mathtex)
        )
        self.wait(2)
        self.play(
            Unwrite(self.cavity_refl_tex)
        )
        self.wait(2)

        # Define the reflectivity of the mirrors and the phase of the roundtrip
        self.play(
            Write(self.input_mirror_tex_group)
        )
        self.wait(1)
        self.play(
            Write(self.end_mirror_tex_group)
        )
        self.wait(1)
        self.play(
            Create(self.phase_graph_group)
        )
        self.wait(1)

        self.play(
            Create(self.refl_phase_graph_group),
            Unwrite(self.refl_tex)
        )
        self.wait(1)

        # Add wave updaters
        self.input_beam.add_updater(wave_updater)
        self.refl_beam.add_updater(wave_updater)
        self.cav_east_beam.add_updater(wave_updater)
        self.cav_west_beam.add_updater(wave_updater)
        self.trans_beam.add_updater(wave_updater)

        # Add mirror updaters
        self.input_mirror.add_updater(
            lambda mob: mob.move_to((x_input_mirror.get_value() - self.mirror_width/2) * RIGHT)
        )
        self.end_mirror.add_updater(
            lambda mob: mob.move_to((x_end_mirror.get_value() + self.mirror_width/2) * RIGHT)
        )
        self.input_mirror.add_updater(
            lambda mob: mob.set_fill(BLUE, opacity=self.input_mirror_refl.get_value()**2)
        )
        self.end_mirror.add_updater(
            lambda mob: mob.set_fill(BLUE, opacity=self.end_mirror_refl.get_value()**2)
        )

        # Add length and phase updaters
        self.cavity_length.add_updater(
            lambda z: z.set_value( x_end_mirror.get_value() - x_input_mirror.get_value() )
        )
        self.phase.add_updater(
            lambda z: z.set_value( 2 * k_input.get_value() * self.cavity_length.get_value() % (2 * PI) )
        )

        # Add refl ring updater
        self.refl_ring.add_updater(
            lambda mob: mob.become(
                make_refl_ring(
                    self.input_mirror_refl.get_value(),
                    self.end_mirror_refl.get_value(),
                    refl_graph_origin,
                    scaler=refl_graph_length
                )
            )
        )

        # Adjust the phase and amplitudes of the beams according to the Fabry Perot equations
        # Add updaters to reflection ValueTrackers which need to be updated based on the cavity length
        # Remember to add these ValueTrackers to the Scene itself with self.add()
        self.add(self.cavity_length)

        self.cavity_length.add_updater(
            lambda z: z.set_value( x_end_mirror.get_value() - x_input_mirror.get_value() )
        )

        self.add(amp_refl_scaler)
        self.add(phase_refl_offset)

        amp_refl_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_refl(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_refl_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_refl(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_cav_east_scaler)
        self.add(phase_cav_east_offset)

        amp_cav_east_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_buildup_toward_end_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_cav_east_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_buildup_toward_end_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_cav_west_scaler)
        self.add(phase_cav_west_offset)

        amp_cav_west_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_buildup_toward_input_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_cav_west_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_buildup_toward_input_mirror(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        self.add(amp_trans_scaler)
        self.add(phase_trans_offset)

        amp_trans_scaler.add_updater(
            lambda z: z.set_value( 
                np.abs( 
                    get_cav_trans(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        phase_trans_offset.add_updater(
            lambda z: z.set_value( 
                np.angle( 
                    get_cav_trans(
                        k_input.get_value() * self.cavity_length.get_value(), 
                        self.input_mirror_refl.get_value(),
                        self.end_mirror_refl.get_value()
                    ) 
                )
            )
        )

        # Play with the roundtrip length/phase
        self.play(
            x_end_mirror.animate.set_value(3.5)
        )
        self.wait(1)
        self.play(
            x_end_mirror.animate.set_value(2.69)
        )
        self.wait(1)
        self.play(
            x_end_mirror.animate.set_value(5.0)
        )
        self.wait(1)
        self.play(
            x_end_mirror.animate.set_value(3.0)
        )
        self.wait(1)

        # Now let's kick this cavity into high finesse territory
        # Clean up the area
        self.play(
            Unwrite(self.cavity_refl_mathtex),
            Unwrite(self.electric_field_refl_5)
        )
        self.play(
            Uncreate(self.phase_graph_group)
        )
        self.wait(1)

        # Add cavity gain tracker
        self.play(
            Write(self.cavity_gain_tex),
            Write(self.cavity_gain_value_group)
        )
        self.wait(3)

        self.play(
            Unwrite(self.cavity_gain_tex),
        )
        self.wait(1)

        # Now scale up the end mirror reflectivity to one (overcoupling the cavity)
        self.play(
            self.end_mirror_refl.animate.set_value(1.0),
        )
        self.wait(3)

        self.play(
            amp_input.animate.set_value(0.6)
        )
        self.wait(1)

        # But can we take it higher?
        self.play(
            self.input_mirror_refl.animate.set_value(np.sqrt(0.9))
        )
        self.wait(2)

        # Scale down the input amplitude
        self.play(
            amp_input.animate.set_value(0.2)
        )
        self.wait(2)

        # But can we take it higher?
        self.play(
            self.input_mirror_refl.animate.set_value(np.sqrt(0.99))
        )
        self.wait(2)

        # # Scale down the input amplitude
        # self.play(
        #     amp_input.animate.set_value(0.1)
        # )

        # # But can we take it higher?
        # self.play(
        #     self.input_mirror_refl.animate.set_value(np.sqrt(0.997))
        # )
        # self.wait(2)

        # Play with the length of the high finesse cavity
        self.play(
            Write(self.length_value_group)
        )
        self.play(
            x_end_mirror.animate.set_value(3.5)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.3)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.2)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.1)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.05)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.03)
        )
        self.wait(1)

        self.play(
            x_end_mirror.animate.set_value(3.0),
            run_time=4.0
        )
        self.wait(1)


