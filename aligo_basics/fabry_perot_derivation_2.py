"""fabry_perot_derivation_2.py
Starts with circulating power derivation,
and explains it's significance.
"""

from manim import *


class FabryPerotDerivation2(Scene):
    def construct(self):

        ## Tex
        final_equation_height = 2 * UP
        
        self.electric_field_sum_6 = MathTex(
            r"{{ E_\mathrm{cav\,east} }} = { {{ \tau_i }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } } {{ E_\mathrm{in} }}"
        )
        self.electric_field_sum_6[0].set_color(PURPLE)
        self.electric_field_sum_6[2].set_color(BLUE)
        self.electric_field_sum_6[4].set_color(BLUE)
        self.electric_field_sum_6[5].set_color(BLUE)
        self.electric_field_sum_6[7].set_color(RED)
        self.electric_field_sum_6.move_to(final_equation_height)

        # Resonance definition
        self.resonance_definition_tex = Tex(
            "Resonance:\\\\Maximize cavity power via constructive interference",
            color=RED
        ).next_to(self.electric_field_sum_6, UP)

        # Resonance condition

        self.refl1 = MathTex(
            r"{{ r_i }} = 0.99"
        ).next_to(self.electric_field_sum_6, DOWN)
        self.refl1[0].set_color(BLUE)

        self.refl2 = MathTex(
            r"{{ r_e }} \approx 1"
        ).next_to(self.refl1, DOWN)
        self.refl2[0].set_color(BLUE)

        self.exponent_1 = MathTex(
            r"{{ e^{ i k (2 L) } }}"
        ).next_to(self.refl2, DOWN).shift(DOWN)
        self.exponent_2 = MathTex(
            r"{{ e^{ i k (2 L) } }} = 1"
        ).move_to(self.exponent_1, DOWN)
        self.exponent_3 = MathTex(
            r" {{ k }} (2 {{ L }})  = 2 {{ \pi }}{{ n }} \qquad {{ \forall \, n \in \mathbb{Z} }}"
        ).move_to(self.exponent_1, DOWN)
        self.exponent_4 = MathTex(
            r" {{ k }}{{ L }} = {{ \pi }}{{ n }}  \qquad {{ \forall \, n \in \mathbb{Z} }}"
        ).move_to(self.exponent_1, DOWN)
        self.exponent_5 = MathTex(
            r" { {{ 2 }} \pi \over {{ \lambda }} } {{ L }} = {{ \pi }}{{ n }}  \qquad {{ \forall \, n \in \mathbb{Z} }}"
        ).move_to(self.exponent_1, DOWN)
        self.exponent_6 = MathTex(
            r" {{ L }} =  { {{ \lambda }} \over {{ 2 }} } {{ n }}  \qquad {{ \forall \, n \in \mathbb{Z} }}"
        ).move_to(self.exponent_1, DOWN)
        self.resonance_condition_tex = Tex(
            "Resonance condition: \\\\Cavity length is equal to half integer wavelength",
            color=YELLOW
        ).next_to(self.exponent_1, DOWN)

        
        ## Scene

        self.add(self.electric_field_sum_6)

        self.wait(2)

        self.play(
            Write(self.resonance_definition_tex)
        )
        self.wait(2)

        self.play(
            Write(self.refl1)
        )
        self.play(
            Write(self.refl2)
        )
        self.wait(2)

        self.play(
            Write(self.exponent_1)
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.exponent_1, self.exponent_2)
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.exponent_2, self.exponent_3)
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.exponent_3, self.exponent_4)
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.exponent_4, self.exponent_5)
        )
        self.wait(2)
        self.play(
            TransformMatchingTex(self.exponent_5, self.exponent_6)
        )
        self.wait(2)

        self.play(
            Write(self.resonance_condition_tex)
        )
        self.wait(5)

        self.play(
            Unwrite(self.resonance_definition_tex),
            Unwrite(self.resonance_condition_tex),
            Unwrite(self.exponent_6),
            Write(self.refl1),
            Write(self.refl2),
        )
        self.wait(1)
