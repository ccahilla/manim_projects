"""fabry_perot_derivation_4.py
Starts with circulating power derivation,
and explains it's significance.
"""

from manim import *

class FabryPerotDerivation4(Scene):
    def construct(self):

        self.color_map = {
            "in" : RED,
            "refl" : ORANGE,
            "cav east" : PURPLE,
            "cav west" : PINK,
            "trans" : YELLOW,
            "mirror" : BLUE,
        }

        ## Tex
        final_equation_height = 2 * UP

        self.electric_field_refl_6 = MathTex(
            r"{{ r_\mathrm{cav} }} = { {{ - r_i }} + {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }"
        ).move_to(final_equation_height)
        self.electric_field_refl_6[0].set_color(self.color_map["refl"])
        self.electric_field_refl_6[2].set_color(self.color_map["mirror"])
        self.electric_field_refl_6[4].set_color(self.color_map["mirror"])
        self.electric_field_refl_6[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_6[8].set_color(self.color_map["mirror"])

        self.electric_field_refl_deriv_1 = MathTex(
            r"{{ { d \over dL } }}{{ r_\mathrm{cav} }} = {{ { d \over dL } }}{ {{ - r_i }} + {{ r_e }}{{ e^{ i k (2 L) } }} \over 1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } }"
        ).next_to(self.electric_field_refl_6, DOWN)
        self.electric_field_refl_deriv_1[1].set_color(self.color_map["refl"])
        self.electric_field_refl_deriv_1[4].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_1[6].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_1[9].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_1[11].set_color(self.color_map["mirror"])

        self.electric_field_refl_deriv_2 = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL }{{ = }}{ i 2 k (1 - {{ r_i }})^2 {{ r_e }}{{ e^{ i k (2 L) } }} \over (1 - {{ r_i }}{{ r_e }} e^{ i k (2 L) } )^2 }"
        ).move_to(self.electric_field_refl_deriv_1)
        self.electric_field_refl_deriv_2[1].set_color(self.color_map["refl"])
        self.electric_field_refl_deriv_2[5].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_2[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_2[10].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_2[11].set_color(self.color_map["mirror"])

        self.electric_field_refl_deriv_3 = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL }{{ = }}{ i 2 k {{ \tau_i }}^2 {{ r_e }} \over ( 1 - {{ r_i }}{{ r_e }} )^2 }"
        ).move_to(self.electric_field_refl_deriv_2).shift(3.0*DOWN + 0.5*RIGHT)
        self.electric_field_refl_deriv_3[1].set_color(self.color_map["refl"])
        self.electric_field_refl_deriv_3[5].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[7].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[9].set_color(self.color_map["mirror"])
        self.electric_field_refl_deriv_3[10].set_color(self.color_map["mirror"])

        # Resonance condition
        self.resonance_condition_tex = Tex(
            "Resonance condition:",
            color=YELLOW
        ).next_to(self.electric_field_refl_deriv_2, DOWN).shift(3*LEFT)
        self.resonance_condition_mathtex_1 = MathTex(
            r"2 k L \rightarrow 2 \pi n \qquad {{ \forall \, n \in \mathbb{Z} }}"
        ).next_to(self.resonance_condition_tex,  DOWN)

        self.resonance_condition_mathtex_2 = MathTex(
            r"e^{ i k (2 L) } \rightarrow 1"
        ).next_to(self.resonance_condition_tex,  DOWN)

        # Conservation of energy
        self.conservation_tex = Tex(
            "Conservation of energy:",
            color=BLUE
        ).next_to(self.electric_field_refl_deriv_2, DOWN).shift(2.5*RIGHT)
        self.conservation_mathtex_1 = MathTex(
            r"{{ r_i }}^2 + {{ \tau_i }}^2 = 1"
        ).next_to(self.conservation_tex, DOWN)
        self.conservation_mathtex_1[0].set_color(self.color_map["mirror"])
        self.conservation_mathtex_1[2].set_color(self.color_map["mirror"])

        self.conservation_mathtex_2 = MathTex(
            r"{{ \tau_i }}^2 = 1 - {{ r_i }}^2"
        ).move_to(self.conservation_mathtex_1)
        self.conservation_mathtex_2[0].set_color(self.color_map["mirror"])
        self.conservation_mathtex_2[2].set_color(self.color_map["mirror"])

        # FP sensitivity to length changes
        self.fabry_perot_response_tex = Tex(
            r"Fabry P\'erot response\\to changes in length",
            color=ORANGE
        ).next_to(self.electric_field_refl_deriv_3, LEFT)

        ## Scene

        self.play(
            Write(self.electric_field_refl_6)
        )
        self.wait(2)

        self.electric_field_refl_6_copy = self.electric_field_refl_6.copy()
        self.add(self.electric_field_refl_6_copy)
        self.play(
            TransformMatchingTex(self.electric_field_refl_6_copy, self.electric_field_refl_deriv_1)
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.electric_field_refl_deriv_1, self.electric_field_refl_deriv_2)
        )
        self.wait(2)

        # Write the resonance condition
        self.play(
            Write(self.resonance_condition_tex),
            Write(self.resonance_condition_mathtex_1)
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.resonance_condition_mathtex_1, self.resonance_condition_mathtex_2)
        )
        self.wait(2)

        # Write the conservation optics equation
        self.play(
            Write(self.conservation_tex),
            Write(self.conservation_mathtex_1)
        )
        self.wait(2)

        self.play(
            TransformMatchingTex(self.conservation_mathtex_1, self.conservation_mathtex_2)
        )
        self.wait(2)

        self.electric_field_refl_deriv_2_copy = self.electric_field_refl_deriv_2.copy()
        self.add(self.electric_field_refl_deriv_2_copy)
        self.play(
            TransformMatchingTex(self.electric_field_refl_deriv_2_copy, self.electric_field_refl_deriv_3)
        )
        self.wait(2)

        self.play(
            Write(self.fabry_perot_response_tex),
            run_time=2
        )
