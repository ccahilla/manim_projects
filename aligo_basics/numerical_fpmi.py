"""numerical_fpmi.py

Derivation of a Fabry Perot Michelson resp
"""

from manim import *

class NumericalFPMI(Scene):
    def construct(self):

        ## Tex
        main_eq_location = 2*UP

        # Derivative trans power
        self.trans_deriv_2 = MathTex(
            r"{ d {{ P_\mathrm{trans} }} \over {{ \Delta L }} }{{ = }} 8 ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L }}{{ P_\mathrm{in} }}"
        ).scale(0.7).move_to(main_eq_location, DOWN)
        self.trans_deriv_2[1].set_color(GREEN)
        self.trans_deriv_2[3].set_color(ORANGE)
        self.trans_deriv_2[7].set_color(ORANGE)
        self.trans_deriv_2[9].set_color(ORANGE)
        self.trans_deriv_2[10].set_color(RED)

        self.trans_deriv_3 = MathTex(
            r"{ d {{ P_\mathrm{trans} }} \over {{ h }} }{{ = }} 8 L ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L }}{{ P_\mathrm{in} }}"
        ).scale(0.7).move_to(main_eq_location, DOWN)
        self.trans_deriv_3[1].set_color(GREEN)
        self.trans_deriv_3[3].set_color(YELLOW)
        self.trans_deriv_3[7].set_color(ORANGE)
        self.trans_deriv_3[9].set_color(ORANGE)
        self.trans_deriv_3[10].set_color(RED)

        self.trans_deriv_3_value = MathTex(
            r"{ d {{ P_\mathrm{trans} }} \over {{ h }} } \approx 3 \times 10^{14}~{ \mathrm{W} \over \mathrm{strain} }"
        ).scale(0.7).move_to(main_eq_location, DOWN)
        self.trans_deriv_3_value[1].set_color(GREEN)
        self.trans_deriv_3_value[3].set_color(YELLOW)

        # Trans power
        self.trans_power_eq_3 = MathTex(
            r"{{ P_\mathrm{trans} }} = 4 ({ d {{ r_\mathrm{cav} }} \over dL })^2{{ \Delta L^2 }}{{ P_\mathrm{in} }}"
        ).scale(0.7).next_to(self.trans_deriv_2, DOWN)
        self.trans_power_eq_3[0].set_color(GREEN)
        self.trans_power_eq_3[2].set_color(ORANGE)
        self.trans_power_eq_3[4].set_color(ORANGE)
        self.trans_power_eq_3[5].set_color(RED)

        self.trans_power_eq_3_value = MathTex(
            r"{{ P_\mathrm{trans} }} \approx 150~\mathrm{milliwatts}"
        ).scale(0.7).next_to(self.trans_deriv_2, DOWN)
        self.trans_power_eq_3_value[0].set_color(GREEN)

        # dr_cav dL
        self.electric_field_refl_deriv_3 = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL }{{ = }}{ i 2 k {{ \tau_i }}^2 {{ r_e }} \over ( 1 - {{ r_i }}{{ r_e }} )^2 }"
        ).scale(0.7).next_to(self.trans_power_eq_3, DOWN)
        self.electric_field_refl_deriv_3[1].set_color(ORANGE)
        self.electric_field_refl_deriv_3[5].set_color(BLUE)
        self.electric_field_refl_deriv_3[7].set_color(BLUE)
        self.electric_field_refl_deriv_3[9].set_color(BLUE)
        self.electric_field_refl_deriv_3[10].set_color(BLUE)

        self.electric_field_refl_deriv_3_value = MathTex(
            r"{ d {{ r_\mathrm{cav} }} \over dL } \approx 3 \times 10^{9} { \mathrm{rad} \over \mathrm{m}}"
        ).scale(0.7).next_to(self.trans_power_eq_3, DOWN)
        self.electric_field_refl_deriv_3_value[1].set_color(ORANGE)



        # GW to length
        self.gw_to_length_tex = Tex(
            "Gravitational wave\\\\to length conversion: "
        ).scale(0.7).next_to(self.trans_deriv_2, RIGHT).shift(RIGHT)
        self.gw_to_length_mathtex = MathTex(
            r"{{ h }} = { {{ \Delta L }} \over L } "
        ).scale(0.7).next_to(self.gw_to_length_tex, DOWN)
        self.gw_to_length_mathtex[0].set_color(YELLOW)
        self.gw_to_length_mathtex[2].set_color(ORANGE)

        # Set up some values
        self.strain_value = MathTex(
            r"{{ h }} \approx 1 \times 10^{-21}"
        ).scale(0.7).move_to(3*UP+4.7*LEFT)
        self.strain_value[0].set_color(YELLOW)

        self.length_value = MathTex(
            r"L = 4 \times 10^{3}~\mathrm{meters}"
        ).scale(0.7).next_to(self.strain_value, DOWN)

        self.darm_offset_value = MathTex(
            r"{{ \Delta L }} = 1 \times 10^{-12}~\mathrm{meters}"
        ).scale(0.7).next_to(self.length_value, DOWN)
        self.darm_offset_value[0].set_color(ORANGE)

        self.end_mirror_refl_value = MathTex(
            r"{{ r_e }} \approx 1"
        ).scale(0.7).next_to(self.darm_offset_value, DOWN)
        self.end_mirror_refl_value[0].set_color(BLUE)

        self.input_mirror_trans_value = MathTex(
            r"{{ \tau_i^2 }} = 1.5\%"
        ).scale(0.7).next_to(self.end_mirror_refl_value, DOWN)
        self.input_mirror_trans_value[0].set_color(BLUE)

        self.input_mirror_refl_value = MathTex(
            r"{{ r_i }} = 0.992"
        ).scale(0.7).next_to(self.input_mirror_trans_value, DOWN)
        self.input_mirror_refl_value[0].set_color(BLUE)

        self.input_power_value = MathTex(
            r"{{ P_\mathrm{in} }} = 1~\mathrm{kilowatts}"
        ).scale(0.7).next_to(self.input_mirror_refl_value, DOWN)
        self.input_power_value[0].set_color(RED)

        self.lambda_value = MathTex(
            r"\lambda = 1064~\mathrm{nanometers}"
        ).scale(0.7).next_to(self.input_power_value, DOWN)
        
        self.k_value = MathTex(
            r"k \approx 6 \times 10^{6}~\mathrm{m}^{-1}"
        ).scale(0.7).next_to(self.lambda_value, DOWN)

        # Final signal strength
        self.final_signal_strength = MathTex(
            r"{{ h }}{ d {{ P_\mathrm{trans} }} \over {{ h }} } \approx 300~\mathrm{nanowatts}"
        ).scale(0.7).move_to(2*DOWN)
        self.final_signal_strength[0].set_color(YELLOW)
        self.final_signal_strength[2].set_color(GREEN)
        self.final_signal_strength[4].set_color(YELLOW)

        self.detectable_tex = Tex(
            "Detectable!",
            color=GREEN
        ).scale(0.7).next_to(self.final_signal_strength, DOWN)

        ###  Scene  ###
        self.play(
            FadeIn(self.trans_deriv_2)
        )
        self.wait(1)
        self.play(
            FadeIn(self.trans_power_eq_3)
        )
        self.wait(1)

        self.play(
            FadeIn(self.electric_field_refl_deriv_3)
        )
        self.wait(1)

        self.play(
            Write(self.gw_to_length_tex)
        )
        self.play(
            Write(self.gw_to_length_mathtex)
        )
        self.wait(1)

        self.gw_to_length_mathtex_copy = self.gw_to_length_mathtex.copy()
        self.play(
            TransformMatchingTex(self.gw_to_length_mathtex_copy, self.trans_deriv_3),
            TransformMatchingTex(self.trans_deriv_2, self.trans_deriv_3),
            run_time=2
        )

        # Start writing values
        self.play(
            Write(self.strain_value)
        )
        self.wait(1)

        self.play(
            Write(self.length_value),
        )
        self.wait(1)

        self.play(
            Write(self.darm_offset_value),
        )
        self.wait(1)

        self.play(
            Write(self.end_mirror_refl_value),
            Write(self.input_mirror_trans_value),
            Write(self.input_mirror_refl_value),
        )
        self.wait(1)

        self.play(
            Write(self.input_power_value),
        )
        self.wait(1)

        self.play(
            Write(self.lambda_value),
            Write(self.k_value)
        )
        self.wait(1)

        # Main equation values
        self.play(
            TransformMatchingTex(self.electric_field_refl_deriv_3, self.electric_field_refl_deriv_3_value)
        )
        self.wait(1)

        self.play(
            TransformMatchingTex(self.trans_power_eq_3, self.trans_power_eq_3_value)
        )
        self.wait(1)

        self.play(
            TransformMatchingTex(self.trans_deriv_3, self.trans_deriv_3_value)
        )
        self.wait(1)

        # Multiply by the largest GW wave yet detected
        self.play(
            Write(self.final_signal_strength),
        )
        self.wait(1)

        self.play(
            Write(self.detectable_tex)
        )
        self.wait(1)