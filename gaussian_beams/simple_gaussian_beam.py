from beamtrace.tracer import BeamTrace
from manim import *


def get_simple_lens_beam_scan(q_input, total_length, lens_position, lens_focal_length, steps=500):
    """Takes in seed beam q-parameter, 
    total length of the beam scan,
    and lens position and focal length,
    Returns (zz, ww, gouy, qq),
    the z-location, beam radius, accumulated gouy phase, and q-parameter along the beam scan.
    """
    length1 = lens_position
    length2 = total_length - lens_position
    if length1 < 0.0:
        length1 = 0.0
    if length2 < 0.0:
        length2 = 0.0

    beam = BeamTrace()

    beam.set_seed_beam(q_input)
    beam.add_space(length1)
    beam.add_lens(lens_focal_length, name="1m Lens")
    beam.add_space(length2)

    zz, ww, gouy, qq = beam.scan_beam(steps=steps)

    return zz, ww, gouy, qq

class ConvexLens(VMobject):
    def __init__(self, start=DOWN, end=UP):
        self.start = start
        self.end = end

    def generate_points(self):

        arc_1 = ArcBetweenPoints(
                start=self.start,
                end=self.end,
                angle=TAU/6
            )

        arc_2 = ArcBetweenPoints(
                start=self.end,
                end=self.start,
                angle=TAU/6
            )

        self.append_points(arc_1.points)
        self.append_points(arc_2.points)

        return

class ConcaveLens(VMobject):
    def generate_points(self):
        line_1 = Line(start=np.array([-0.5, 1, 0]), end=np.array([0.5, 1, 0]))

        arcBetweenPoints_1 = ArcBetweenPoints(start=np.array([0.5, 1, 0]),
                                              end=np.array([0.5, -1, 0]),
                                              angle=TAU / 6)

        line_2 = Line(start=np.array([0.5, -1, 0]), end=np.array([-0.5, -1, 0]))

        arcBetweenPoints_2 = ArcBetweenPoints(start=np.array([-0.5, -1, 0]),
                                              end=np.array([-0.5, 1, 0]),
                                              angle=TAU / 6)

        self.append_points(line_1.points)
        self.append_points(arcBetweenPoints_1.points)
        self.append_points(line_2.points)
        self.append_points(arcBetweenPoints_2.points)

        self.add(line_1, arcBetweenPoints_1, line_2, arcBetweenPoints_2)

        return

class SimpleGaussianBeam(Scene):
    """Display the spatial mode of a focused Gaussian beam.
    """
    def make_beam_radius_object(
        self, 
        zz,
        ww,
        origin=np.array([0.0, 0.0, 0.0]),
        color=RED,
        beam_radius_multiplier=1000.0,
    ):
        """Makes a beam radius object, 
        which is just a group of lines connected together.
        """
        beam_radius = VGroup()
        beam_radius.number_of_lines = len(zz)
        beam_radius.color = color
        

        for z1, z2, w1, w2 in zip(zz[:-1], zz[1:], ww[:-1], ww[1:]):

            # Define the Lines
            temp_start = np.array([z1, beam_radius_multiplier * w1, 0])
            temp_end = np.array([z2, beam_radius_multiplier * w2, 0])
            temp_line_up = Line(temp_start, temp_end, color=color)

            temp_start = np.array([z1, -beam_radius_multiplier * w1, 0])
            temp_end = np.array([z2, -beam_radius_multiplier * w2, 0])
            temp_line_down = Line(temp_start, temp_end, color=color)

            beam_radius.add(temp_line_up)
            beam_radius.add(temp_line_down)

        beam_radius.move_to( origin )

        return beam_radius

    def make_convex_lens(self, start=DOWN, end=UP, angle=TAU/6, fill_color=BLUE, color=BLUE):
        lens = VGroup()

        arc_1 = ArcBetweenPoints(
                start=start,
                end=end,
                angle=angle,
                fill_color=fill_color, 
                color=color
            )

        arc_2 = ArcBetweenPoints(
                start=end,
                end=start,
                angle=angle,
                fill_color=fill_color, 
                color=color
            )
        lens.add(arc_1, arc_2)

        return lens

    def construct(self):
        # Set up a timer variable
        self.timer = 0.0
        self.rate = 0.20
        number_of_lines = 100

        ## Define an input beam and lens position on screen
        q_input = ComplexValueTracker(-6.0+1.0j)
        beam_z_start = ValueTracker(-6.0)
        beam_z_end = ValueTracker(6.0)
        beam_radius_multiplier = 750.0

        lens_position = ValueTracker(beam_z_end.get_value())
        lens_focal_length = ValueTracker(1.0)

        total_length = beam_z_end.get_value() - beam_z_start.get_value()
        relative_lens_position = lens_position.get_value() - beam_z_start.get_value()
        zz, ww, gouy, qq = get_simple_lens_beam_scan(q_input.get_value(), total_length, relative_lens_position, lens_focal_length.get_value())
        input_beam_radius = self.make_beam_radius_object(zz, ww, beam_radius_multiplier=beam_radius_multiplier)

        ## Lens
        lens_start = 3 * DOWN
        lens_end = 3 * UP
        lens_angle = TAU / 18
        lens = self.make_convex_lens(start=lens_start, end=lens_end, angle=lens_angle)
        lens.add_updater(
            lambda z: z.set_x( lens_position.get_value() )
        )

        ## Tex
        gaussian_beam_tex = Tex(r"Gaussian Beam Scan").shift(3.5*UP)

        # Length as Decimal Number
        lens_position_tex = Tex(r"$z_\mathrm{lens} = $", color=BLUE)
        lens_position_tex_value = always_redraw(
            lambda: DecimalNumber(num_decimal_places=1, include_sign=True, color=BLUE)
            .set_value(lens_position.get_value())
            .next_to(lens_position_tex, RIGHT, buff=0.1)
        )
        lens_position_group = VGroup(lens_position_tex, lens_position_tex_value)
        lens_position_group.add_updater(
            lambda z: z.next_to(lens, UP, buff=0.2)
        )

        ## Updaters
        def time_updater(mob, dt):
            self.timer += dt * self.rate
            return

        def beam_radius_updater(mob):
            total_length = beam_z_end.get_value() - beam_z_start.get_value()
            relative_lens_position = lens_position.get_value() - beam_z_start.get_value()
            zz, ww, gouy, qq = get_simple_lens_beam_scan(q_input.get_value(), total_length, relative_lens_position, lens_focal_length.get_value())
            temp_mob = self.make_beam_radius_object(zz, ww, beam_radius_multiplier=beam_radius_multiplier)
            mob.become(temp_mob)
            return 

        # Add updaters to reflection ValueTrackers which need to be updated based on the cavity length
        # Remember to add these ValueTrackers to the Scene itself with self.add()
        self.add(q_input)
        self.add(beam_z_start)
        self.add(beam_z_end)
        self.add(lens_position)
        self.add(lens_focal_length)

        self.add(input_beam_radius)

        self.play(Create(lens))
        self.wait(1)

        self.play(Create(lens_position_group))
        self.wait(1)

        input_beam_radius.add_updater(beam_radius_updater)

        self.play(lens_position.animate.set_value(-2.0), run_time=4)
        self.wait(2)

        return
