# gaussian_beams

## Instructions

0. Install Anaconda, and install the `manim` conda `environment.yml` in the above directory:

```
conda env create -f environment.yml
conda activate manim
```

1. In the `manim` conda environment, compile the simple Gaussian beam example:

```
manim render simple_gaussian_beam.py SimpleGaussianBeam -ql
```

`manim` is the python module,  
`render` is the module command,  
(`manim render --help` will show the manim render command arguments).  
`simple_gaussian_beam.py` is the python script which houses the Scene class we want to render,  
and `SimpleGaussianBeam` is the Scene class we want to render.  
The `-ql` stands for low quality, this can be removed to compile the movie in high quality, high frames per second,
but this takes longer.
