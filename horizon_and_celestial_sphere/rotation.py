from manim import *

class ThreeDCameraRotation(ThreeDScene):
    def construct(self):
        # self.camera.background_color = WHITE

        camera_phi = 65
        camera_theta = 115
        self.set_camera_orientation(phi=camera_phi * DEGREES, theta=camera_theta * DEGREES)
        
        phi, theta, focal_distance, gamma, zoom = self.camera.get_value_trackers()

        # Radius of the Celestial Sphere
        rr_s = 3.75
        sphere = Sphere(radius=rr_s, resolution=30).set_color(GREY).set_opacity(0.1) #.shift(RIGHT)

        # Sphere Text
        sphere_text = Text("Celestial Sphere").set_color(GREY).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)

        sphere_text.move_to((rr_s + 0.5) * UP)

        # The latitude of the observer on the Earth
        # phi_e = 60 * np.pi / 180 # radians
        phi_e = ValueTracker(60) # degrees
        theta_e = ValueTracker(0) # degrees

        # Decimal Number and associated Tex
        # latitude_tex = Tex(
        #     r"Latitude $\theta = $"
        # )

        # latitude_value = DecimalNumber(
        #     0,
        #     num_decimal_places=0, 
        #     include_sign=False, 
        #     color=WHITE
        # )#.next_to(latitude_tex, RIGHT, buff=0.1)
            
        # # latitude_tex_2 = Tex(r"${}^\circ$").next_to(latitude_value, RIGHT, buff=0.1)

        # def latitude_value_updater(mob):
        #     temp_mob = DecimalNumber(
        #         phi_e.get_value(),
        #         num_decimal_places=0, 
        #         include_sign=False, 
        #         color=WHITE
        #     )
        #     mob.become(temp_mob)

            
        
        # latitude_value.add_updater(latitude_value_updater)

        # # text3d=Text("This is a 3D text")

        # # self.add_fixed_in_frame_mobjects(text3d) #<----- Add this
        # # text3d.to_corner(UL)

        # # latitude_group = VGroup(
        # #     # text3d,
        # #     latitude_tex,
        # #     latitude_value,
        # #     # latitude_tex_2
        # # )

        # self.add_fixed_in_frame_mobjects(latitude_value)
        # latitude_value.to_corner(UR)

        # The radius of the Earth
        rr_e = 0.75

        # Updaters for the Person's location on earth
        def zz_e_updater(mob):
            mob.set_value(rr_e * np.sin(phi_e.get_value() * DEGREES))
        def yy_e_updater(mob):
            mob.set_value(rr_e * np.cos(phi_e.get_value() * DEGREES) * np.cos(theta_e.get_value() * DEGREES) )
        def xx_e_updater(mob):
            mob.set_value(rr_e * np.cos(phi_e.get_value() * DEGREES) * np.sin(theta_e.get_value() * DEGREES) )

        zz_e = ValueTracker(0)
        yy_e = ValueTracker(0)
        xx_e = ValueTracker(0)

        zz_e.add_updater(zz_e_updater)
        yy_e.add_updater(yy_e_updater)
        xx_e.add_updater(xx_e_updater)

        # Earth and Person system
        earth = Sphere(radius=rr_e).set_color(BLUE).set_opacity(0.2)
        person = Sphere(radius=0.05, resolution=10).set_color(ORANGE)
        person.move_to
        def person_updater(mob):
            mob.move_to(xx_e.get_value() * RIGHT + yy_e.get_value() * UP + zz_e.get_value() * OUT)

        person.add_updater(person_updater)


        # Earth and Person Text
        earth_text = Text("Earth").set_color(BLUE).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)
        person_text = Text("Person").set_color(ORANGE).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)

        earth_text.move_to(1.5 * OUT)
        person_text.move_to(person.get_center() + RIGHT)

        earth_person = VGroup(
            earth,
            person
        )

        num_stars = 25
        stars = []
        for ii in range(num_stars):
            
            theta_s = np.pi * np.random.rand()
            phi_s = 2 * np.pi * np.random.rand()
            zz_s = rr_s * np.cos(theta_s)
            xx_s = rr_s * np.cos(phi_s) * np.sin(theta_s)
            yy_s = rr_s * np.sin(phi_s) * np.sin(theta_s)
            star = Sphere(radius=0.01, resolution=3).set_color(YELLOW).shift(xx_s*RIGHT + yy_s*UP + zz_s*OUT)
            stars.append(star)


        # axes = ThreeDAxes(
        #     x_range=None, 
        #     y_range=None, 
        #     z_range=None, 
        #     x_length=3, 
        #     y_length=3, 
        #     z_length=3, 
        #     z_axis_config=None, 
        #     z_normal=np.array([0., -1., 0.])
        # ).shift(aa_e*UP + zz_e*OUT).rotate(phi_e * DEGREES, UP)

        # Zenith, North, East Lines
        line_size = 2
        zenith_color = YELLOW_B
        north_color = RED_B
        east_color = GREEN_B

        def zenith_line_updater(mob):
            temp_mob = Arrow3D( person.get_center(), rr_s / rr_e * person.get_center() ).set_color(zenith_color)
            mob.become(temp_mob)
        def north_line_updater(mob):
            theta_hat = line_size * (\
                - np.sin(phi_e.get_value() * DEGREES) * np.sin(theta_e.get_value() * DEGREES) * RIGHT \
                - np.sin(phi_e.get_value() * DEGREES) * np.cos(theta_e.get_value() * DEGREES) * UP \
                + np.cos(phi_e.get_value() * DEGREES) * OUT \
            )
            temp_mob = Arrow3D(person.get_center(), person.get_center() + theta_hat).set_color(north_color)
            mob.become(temp_mob)
        def east_line_updater(mob):
            phi_hat = line_size * (\
                - np.cos(theta_e.get_value() * DEGREES) * RIGHT \
                + np.sin(theta_e.get_value() * DEGREES) * UP 
            )
            temp_mob = Arrow3D(person.get_center(), person.get_center() + phi_hat).set_color(east_color)
            mob.become(temp_mob)

        zenith_line = Arrow3D( person.get_center(), rr_s / rr_e * person.get_center() ).set_color(zenith_color)

        north_line = Arrow3D(
            person.get_center(), line_size * person.get_center() 
        ).set_color(north_color).rotate(90*DEGREES, RIGHT, person.get_center())
        east_line = Arrow3D(
            person.get_center(), line_size * person.get_center() 
        ).set_color(east_color).rotate(90*DEGREES, RIGHT, person.get_center()).rotate(-90*DEGREES, person.get_center(), person.get_center())

        zenith_line.add_updater(zenith_line_updater)
        north_line.add_updater(north_line_updater)
        east_line.add_updater(east_line_updater)

        # Zenith, North, and East Text
        zenith_line_text = Text("Zenith").set_color(zenith_color).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)
        north_line_text = Text("North").set_color(north_color).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)
        east_line_text = Text("East").set_color(east_color).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)

        text_length = 2.6
        zenith_line_text.add_updater(
            lambda mob: mob.move_to(text_length * (zenith_line.get_center() - person.get_center()) + person.get_center())
        )
        north_line_text.add_updater(
            lambda mob: mob.move_to(text_length * (north_line.get_center() - person.get_center()) + person.get_center())
        )
        east_line_text.add_updater(
            lambda mob: mob.move_to(text_length * (east_line.get_center() - person.get_center()) + person.get_center())
        )




        # Horizon
        horizon_size = 2

        def horizon_updater(mob):
            # Two vectors and a point to define the plane
            theta_hat = horizon_size * (\
                - np.sin(phi_e.get_value() * DEGREES) * np.sin(theta_e.get_value() * DEGREES) * RIGHT \
                - np.sin(phi_e.get_value() * DEGREES) * np.cos(theta_e.get_value() * DEGREES) * UP \
                + np.cos(phi_e.get_value() * DEGREES) * OUT \
            )
            phi_hat = horizon_size * (\
                - np.cos(theta_e.get_value() * DEGREES) * RIGHT \
                + np.sin(theta_e.get_value() * DEGREES) * UP 
            )
            temp_mob = Surface(
                lambda u, v: np.array([
                    person.get_x() + theta_hat[0] * u + phi_hat[0] * v,
                    person.get_y() + theta_hat[1] * u + phi_hat[1] * v,
                    person.get_z() + theta_hat[2] * u + phi_hat[2] * v,
                ]),
                u_range=(-1, 1),
                v_range=(-1, 1),
                resolution=10
            ).set_opacity(0.2).set_color(RED) #.shift(person.get_center())
            mob.become(temp_mob)

        horizon = Surface(
            lambda u, v: np.array([
                horizon_size * u,
                horizon_size * v,
                0
            ]),
            u_range=(-1, 1),
            v_range=(-1, 1)
        ).set_opacity(0.2).set_color(RED) #.shift(person.get_center()).rotate(phi_e.get_value() + 90 * DEGREES, RIGHT, person.get_center())

        horizon.add_updater(horizon_updater)    
        
        # Horizon Text
        horizon_text = Text("Horizon", font_size=30).set_color(RED).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)

        def horizon_text_updater(mob):
            # Two vectors and a point to define the plane
            theta_hat = horizon_size * (\
                - np.sin(phi_e.get_value() * DEGREES) * np.sin(theta_e.get_value() * DEGREES) * RIGHT \
                - np.sin(phi_e.get_value() * DEGREES) * np.cos(theta_e.get_value() * DEGREES) * UP \
                + np.cos(phi_e.get_value() * DEGREES) * OUT \
            )
            phi_hat = horizon_size * (\
                - np.cos(theta_e.get_value() * DEGREES) * RIGHT \
                + np.sin(theta_e.get_value() * DEGREES) * UP 
            )
            temp_mob = Text("Horizon", font_size=24).set_color(RED).rotate_about_origin(-90*DEGREES, RIGHT).rotate_about_origin(180*DEGREES, UP)
            temp_mob.rotate_about_origin(phi_e.get_value() * DEGREES, RIGHT)
            temp_mob.rotate_about_origin(-theta_e.get_value() * DEGREES, OUT)
            temp_mob.move_to(person.get_center() + 0.75 * (theta_hat + phi_hat))
            mob.become(temp_mob)

        text_length = 2.6
        horizon_text.add_updater(horizon_text_updater)
        # horizon_text.add_updater(
        #     lambda mob: mob.move_to(text_length * (horizon.get_center() - person.get_center()) + person.get_center())
        # )


        earth_person_line = VGroup(
            earth,
            person,
            zenith_line,
            north_line,
            east_line,
            horizon
        )

        # Add ValueTrackers to the Scene
        self.add(phi_e, theta_e, zz_e, yy_e, xx_e)

        self.play(FadeIn(earth))
        self.wait(0.5)
        self.play(Write(earth_text))
        self.wait(0.5)

        self.play(FadeIn(person))
        self.wait(0.5)
        self.play(Write(person_text))
        self.wait(0.5)

        self.play(FadeIn(sphere))
        self.wait(0.5)
        self.play(Write(sphere_text))
        self.wait(0.5)

        self.play(Unwrite(earth_text), Unwrite(person_text))
        self.wait(0.5)

        for ii in range(num_stars):
            star = stars[ii]
            self.play(FadeIn(star), run_time=0.1)
        self.wait(0.5)

        self.play(FadeIn(zenith_line))
        self.wait(1)
        self.play(FadeIn(north_line))
        self.wait(1)
        self.play(FadeIn(east_line))
        self.wait(1)
        self.play(Write(zenith_line_text))
        self.wait(1)
        self.play(Write(north_line_text))
        self.wait(1)
        self.play(Write(east_line_text))
        self.wait(1)

        self.play(FadeIn(horizon))
        self.wait(1)
        self.play(Write(horizon_text))
        self.wait(1)

        self.play(
            phi_e.animate.set_value(15)#, run_time=1, #rate_func=linear
        )
        self.wait(1)
        self.play(
            theta_e.animate.set_value(-360), run_time=10, rate_func=linear
        )
        self.wait(1)

        self.play(
            phi_e.animate.set_value(-30)#, run_time=1, #rate_func=linear
        )
        self.wait(1)

        self.play(
            phi_e.animate.set_value(90)#, run_time=1, #rate_func=linear
        )
        self.wait(1)

        self.play(
            phi_e.animate.set_value(60)#, run_time=1, #rate_func=linear
        )
        self.wait(1)

        self.play(
            theta_e.animate.set_value(-2*360), run_time=10, rate_func=linear
        )
        self.wait(1)

        
        # self.play(
        #     Rotate(earth_person_line, 360*DEGREES, OUT, ORIGIN),
        #     run_time=12,
        #     rate_func=linear
        # )
        # self.move_camera(frame_center=rr_e * OUT)
        # self.wait()

        # zoom_ratio = 10 
        # self.play(
        #     # focal_distance.animate.set_value(0),
        #     zoom.animate.set_value(zoom_ratio)
        # )
        # self.wait()

        # self.play(
        #     phi.animate.set_value(phi.get_value() + 90 * DEGREES), 
        #     run_time=3
        # )
        

        self.wait()